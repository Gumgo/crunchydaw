/*
 * ModuleBehavior.h
 * ----------------
 * Description: The base class for the behavior component of a module, which defines how data and GUI interact.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef MODULE_BEHAVIOR_H__DEFINED__
#define MODULE_BEHAVIOR_H__DEFINED__

#include "Source/Common/Common.h"

#include <boost/uuid/uuid.hpp>

class ModuleBehavior : private boost::noncopyable {
public:
	ModuleBehavior( const boost::uuids::uuid &uuid );
	virtual ~ModuleBehavior();

	// Returns UUID identifying this module
	const boost::uuids::uuid &getUuid() const;

private:
	// Unique identifier for this module
	boost::uuids::uuid uuid;
};

#endif // MODULE_BEHAVIOR_H__DEFINED__
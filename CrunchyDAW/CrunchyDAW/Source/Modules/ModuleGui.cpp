#include "ModuleGui.h"

ModuleGui::ModuleGui( const boost::uuids::uuid &uuid )
	: uuid( uuid ) {
}

ModuleGui::~ModuleGui() {
}

const boost::uuids::uuid &ModuleGui::getUuid() const {
	return uuid;
}
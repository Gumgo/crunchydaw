#include "ModuleBehavior.h"

ModuleBehavior::ModuleBehavior( const boost::uuids::uuid &uuid )
	: uuid( uuid ) {
}

ModuleBehavior::~ModuleBehavior() {
}

const boost::uuids::uuid &ModuleBehavior::getUuid() const {
	return uuid;
}
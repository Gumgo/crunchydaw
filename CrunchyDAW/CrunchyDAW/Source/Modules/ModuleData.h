/*
 * ModuleData.h
 * ------------
 * Description: The base class for the data component of a module, which stores all state of the module.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef MODULE_DATA_H__DEFINED__
#define MODULE_DATA_H__DEFINED__

#include "Source/Common/Common.h"

#include <vector>
#include <boost/uuid/uuid.hpp>

struct ModuleDataSlotIdentifier {
	enum Type {
		TYPE_INPUT,
		TYPE_OUTPUT
	};

	// The constructor zeros memory so that hashing won't be affected by uninitialized padding
	ModuleDataSlotIdentifier();

	bool operator==( const ModuleDataSlotIdentifier &other ) const;

	boost::uuids::uuid moduleUuid;
	uint32 slotIndex;
	Type type;
};

size_t hash_value( const ModuleDataSlotIdentifier &moduleDataSlotIdentifier );

struct ModuleDataSlotConnection {
	// The constructor zeros memory so that hashing won't be affected by uninitialized padding
	ModuleDataSlotConnection();

	bool operator==( const ModuleDataSlotConnection &other ) const;

	boost::uuids::uuid outputModuleUuid;	// UUID of the module which is outputting to this connection
	uint32 outputSlotIndex;					// Index of the slot in the output module

	boost::uuids::uuid inputModuleUuid;		// UUID of the module which is inputting from this connection
	uint32 inputSlotIndex;					// Index of the slot in the input module
};

size_t hash_value( const ModuleDataSlotConnection &moduleDataSlotConnection );

// Represents a "slot" (either an input or an output) on a module where data is transferred
class ModuleDataSlot {
public:
	enum Format {
		FORMAT_AUDIO,
		FORMAT_AUTOMATION,
		FORMAT_NOTES,

		FORMAT_COUNT
	};

	ModuleDataSlot( const std::wstring &name, Format format );

	const std::wstring &getSlotName() const;
	Format getFormat() const;

private:
	std::wstring name;
	Format format;
};

class ModuleData : private boost::noncopyable {
public:
	ModuleData( const boost::uuids::uuid &uuid );
	virtual ~ModuleData();

	// Returns UUID identifying this module
	const boost::uuids::uuid &getUuid() const;

	// Display name of the module
	void setDisplayName( const std::wstring &name );
	const std::wstring &getDisplayName() const;

	// Display description of the module - set by the subclass
	const std::wstring &getDisplayDescription() const;

	// Accessors to the module's input slots
	size_t getInputSlotCount() const;
	const ModuleDataSlot &getInputSlot( size_t slotIndex ) const;

	// Accessors to the module's output slots
	size_t getOutputSlotCount() const;
	const ModuleDataSlot &getOutputSlot( size_t slotIndex ) const;

	// Sets/gets canvas dimensions - w/h are clamped to MIN_CANVAS_SIZE
	static const int32 MIN_CANVAS_SIZE = 32;
	void setCanvasDimensions( int32 x, int32 y, int32 w, int32 h );
	void getCanvasDimensions( int32 &x, int32 &y, int32 &w, int32 &h ) const; 

protected:
	void setDisplayDescription( const std::wstring &description );

	void addInputSlot( const ModuleDataSlot &slot, size_t index = INDEX_NONE<size_t>::value );
	void addOutputSlot( const ModuleDataSlot &slot, size_t index = INDEX_NONE<size_t>::value );
	void removeInputSlot( size_t index );
	void removeOutputSlot( size_t index );

private:
	// Unique identifier for this module
	boost::uuids::uuid uuid;

	std::wstring displayName;
	std::wstring displayDescription;

	int32 canvasX, canvasY;
	int32 canvasW, canvasH;

	// Module's slots for input/output of data, provided by derived class
	std::vector<ModuleDataSlot> inputSlots;
	std::vector<ModuleDataSlot> outputSlots;
};

#endif // MODULE_DATA_H__DEFINED__
#include "ModuleData.h"

ModuleDataSlotIdentifier::ModuleDataSlotIdentifier() {
	ZERO_STRUCT( this );
}

bool ModuleDataSlotIdentifier::operator==( const ModuleDataSlotIdentifier &other ) const {
	return
		moduleUuid == other.moduleUuid &&
		slotIndex == other.slotIndex &&
		type == other.type;
}

size_t hash_value( const ModuleDataSlotIdentifier &moduleDataSlotIdentifier ) {
	return boost::hash_range( &moduleDataSlotIdentifier, &moduleDataSlotIdentifier + 1 );
}

ModuleDataSlotConnection::ModuleDataSlotConnection() {
	ZERO_STRUCT( this );
}

bool ModuleDataSlotConnection::operator==( const ModuleDataSlotConnection &other ) const {
	return
		outputModuleUuid == other.outputModuleUuid &&
		outputSlotIndex == other.outputSlotIndex &&
		inputModuleUuid == other.inputModuleUuid &&
		inputSlotIndex == other.inputSlotIndex;
}

size_t hash_value( const ModuleDataSlotConnection &moduleDataSlotConnection ) {
	return boost::hash_range( &moduleDataSlotConnection, &moduleDataSlotConnection + 1 );
}

ModuleDataSlot::ModuleDataSlot( const std::wstring &name, Format format )
	: name( name )
	, format( format ) {
}

const std::wstring &ModuleDataSlot::getSlotName() const {
	return name;
}

ModuleDataSlot::Format ModuleDataSlot::getFormat() const {
	return format;
}

ModuleData::ModuleData( const boost::uuids::uuid &uuid )
	: uuid( uuid ) {
	canvasX = 0;
	canvasY = 0;
	canvasW = MIN_CANVAS_SIZE;
	canvasH = MIN_CANVAS_SIZE;
}

ModuleData::~ModuleData() {
}

const boost::uuids::uuid &ModuleData::getUuid() const {
	return uuid;
}

void ModuleData::setDisplayName( const std::wstring &name ) {
	displayName = name;
}

const std::wstring &ModuleData::getDisplayName() const {
	return displayName;
}

const std::wstring &ModuleData::getDisplayDescription() const {
	return displayDescription;
}

void ModuleData::setCanvasDimensions( int32 x, int32 y, int32 w, int32 h ) {
	canvasX = x;
	canvasY = y;
	canvasW = std::max( canvasW, MIN_CANVAS_SIZE );
	canvasH = std::max( canvasH, MIN_CANVAS_SIZE );
}

void ModuleData::getCanvasDimensions( int32 &x, int32 &y, int32 &w, int32 &h ) const {
	x = canvasX;
	y = canvasY;
	w = canvasW;
	h = canvasH;
}

size_t ModuleData::getInputSlotCount() const {
	return inputSlots.size();
}

const ModuleDataSlot &ModuleData::getInputSlot( size_t slotIndex ) const {
	return inputSlots[slotIndex];
}

size_t ModuleData::getOutputSlotCount() const {
	return outputSlots.size();
}

const ModuleDataSlot &ModuleData::getOutputSlot( size_t slotIndex ) const {
	return outputSlots[slotIndex];
}

void ModuleData::setDisplayDescription( const std::wstring &description ) {
	displayDescription = description;
}

void ModuleData::addInputSlot( const ModuleDataSlot &slot, size_t index ) {
	if (index == INDEX_NONE<size_t>::value) {
		inputSlots.push_back( slot );
	} else {
		inputSlots.insert( inputSlots.begin() + index, slot );
	}
}

void ModuleData::addOutputSlot( const ModuleDataSlot &slot, size_t index ) {
	if (index == INDEX_NONE<size_t>::value) {
		outputSlots.push_back( slot );
	} else {
		outputSlots.insert( outputSlots.begin() + index, slot );
	}
}

void ModuleData::removeInputSlot( size_t index ) {
	inputSlots.erase( inputSlots.begin() + index );
}

void ModuleData::removeOutputSlot( size_t index ) {
	outputSlots.erase( outputSlots.begin() + index );
}
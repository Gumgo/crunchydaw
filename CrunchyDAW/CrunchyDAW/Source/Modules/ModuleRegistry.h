/*
 * ModuleRegistry.h
 * ----------------
 * Description: Allows module types to be associated with unique identifiers.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef MODULE_REGISTRY_H__DEFINED__
#define MODULE_REGISTRY_H__DEFINED__

#include "Source/Common/Common.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/unordered_map.hpp>

class ModuleData;
class ModuleGui;
class ModuleBehavior;

// Specifies properties of a module and provides functions to create its components
struct RegisteredModule {
	typedef fastdelegate::FastDelegate1<const boost::uuids::uuid &, ModuleData *> DataCreator;
	typedef fastdelegate::FastDelegate1<const boost::uuids::uuid &, ModuleGui *> GuiCreator;
	typedef fastdelegate::FastDelegate1<const boost::uuids::uuid &, ModuleBehavior *> BehaviorCreator;

	RegisteredModule(
		const wchar_t *name,
		DataCreator dataCreator,
		GuiCreator guiCreator,
		BehaviorCreator behaviorCreator );

	const wchar_t	*name;
	DataCreator		dataCreator;
	GuiCreator		guiCreator;
	BehaviorCreator	behaviorCreator;
};

// All modules are registered at startup
class ModuleRegistry {
public:
	static const char *NAMESPACE_UUID;

	void registerModules();

	boost::uuids::uuid getModuleUuid( const wchar_t *name ) const;
	const RegisteredModule *getModuleByUuid( const boost::uuids::uuid &uuid ) const;
	const RegisteredModule *getModuleByName( const wchar_t *name ) const;

private:
	void registerModule( const RegisteredModule &registeredModule );

	boost::uuids::uuid namespaceUuid;

	typedef boost::unordered_map<boost::uuids::uuid, RegisteredModule> ModuleMap;
	ModuleMap modules;
};

#endif // MODULE_REGISTRY_H__DEFINED__
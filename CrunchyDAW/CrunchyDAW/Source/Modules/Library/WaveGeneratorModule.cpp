#include "WaveGeneratorModule.h"

const wchar_t *WaveGeneratorModuleData::NAME = L"WaveGeneratorModule";

WaveGeneratorModuleData::WaveGeneratorModuleData( const boost::uuids::uuid &uuid )
	: ModuleData( uuid ) {
}

WaveGeneratorModuleGui::WaveGeneratorModuleGui( const boost::uuids::uuid &uuid )
	: ModuleGui( uuid ) {
}

WaveGeneratorModuleBehavior::WaveGeneratorModuleBehavior( const boost::uuids::uuid &uuid )
	: ModuleBehavior( uuid ) {
}
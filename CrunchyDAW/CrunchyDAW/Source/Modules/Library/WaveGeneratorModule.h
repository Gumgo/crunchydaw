/*
 * WaveGeneratorModule.h
 * ---------------------
 * Description: Module which generates simple waves.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef WAVE_GENERATOR_MODULE_H__DEFINED__
#define WAVE_GENERATOR_MODULE_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/Modules/ModuleData.h"
#include "Source/Modules/ModuleGui.h"
#include "Source/Modules/ModuleBehavior.h"

class WaveGeneratorModuleData : public ModuleData {
public:
	static const wchar_t *NAME;

	WaveGeneratorModuleData( const boost::uuids::uuid &uuid );
};

class WaveGeneratorModuleGui : public ModuleGui {
public:
	WaveGeneratorModuleGui( const boost::uuids::uuid &uuid );
};

class WaveGeneratorModuleBehavior : public ModuleBehavior {
public:
	WaveGeneratorModuleBehavior( const boost::uuids::uuid &uuid );
};

#endif // WAVE_GENERATOR_MODULE_H__DEFINED__
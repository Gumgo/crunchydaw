#include "ModuleRegistry.h"

// Include modules here
#include "Library/WaveGeneratorModule.h"

const char *ModuleRegistry::NAMESPACE_UUID = "b2472bd0-e52b-11e3-ac10-0800200c9a66";

template<typename T> ModuleData *moduleDataCreator( const boost::uuids::uuid &uuid ) {
	return new T( uuid );
}

template<typename T> ModuleGui *moduleGuiCreator( const boost::uuids::uuid &uuid ) {
	return new T( uuid );
}

template<typename T> ModuleBehavior *moduleBehaviorCreator( const boost::uuids::uuid &uuid ) {
	return new T( uuid );
}

RegisteredModule::RegisteredModule(
	const wchar_t *name,
	DataCreator dataCreator,
	GuiCreator guiCreator,
	BehaviorCreator behaviorCreator )
	: name( name )
	, dataCreator( dataCreator )
	, guiCreator( guiCreator )
	, behaviorCreator( behaviorCreator ) {
}

void ModuleRegistry::registerModules() {
	boost::uuids::string_generator namespaceGenerator;
	namespaceUuid = namespaceGenerator( NAMESPACE_UUID );

	// Register all modules here
	registerModule( RegisteredModule(
		WaveGeneratorModuleData::NAME,
		&moduleDataCreator<WaveGeneratorModuleData>,
		&moduleGuiCreator<WaveGeneratorModuleGui>,
		&moduleBehaviorCreator<WaveGeneratorModuleBehavior> ) );
}

void ModuleRegistry::registerModule( const RegisteredModule &registeredModule ) {
	boost::uuids::name_generator generator( namespaceUuid );
	boost::uuids::uuid uuid = generator( registeredModule.name );

	// Collision unlikely, but check with an assert (would occur if two modules are named the same)
	BOOST_ASSERT( modules.find( uuid ) == modules.end() );

	modules.insert( std::make_pair( uuid, registeredModule ) );
}

boost::uuids::uuid ModuleRegistry::getModuleUuid( const wchar_t *name ) const {
	boost::uuids::name_generator generator( namespaceUuid );
	boost::uuids::uuid uuid = generator( name );
	BOOST_ASSERT( modules.find( uuid ) != modules.end() );
	return uuid;
}

const RegisteredModule *ModuleRegistry::getModuleByUuid( const boost::uuids::uuid &uuid ) const {
	const RegisteredModule *result = NULL;
	ModuleMap::const_iterator it = modules.find( uuid );
	if (it != modules.end()) {
		result = &it->second;
	}

	return result;
}

const RegisteredModule *ModuleRegistry::getModuleByName( const wchar_t *name ) const {
	return getModuleByUuid( getModuleUuid( name ) );
}
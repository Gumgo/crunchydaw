/*
 * ModuleGui.h
 * -----------
 * Description: The base class for the GUI component of a module, which defines what is visible to the user.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef MODULE_GUI_H__DEFINED__
#define MODULE_GUI_H__DEFINED__

#include "Source/Common/Common.h"

#include <boost/uuid/uuid.hpp>

class ModuleGui : private boost::noncopyable {
public:
	ModuleGui( const boost::uuids::uuid &uuid );
	virtual ~ModuleGui();

	// Returns UUID identifying this module
	const boost::uuids::uuid &getUuid() const;

private:
	// Unique identifier for this module
	boost::uuids::uuid uuid;
};

#endif // MODULE_GUI_H__DEFINED__
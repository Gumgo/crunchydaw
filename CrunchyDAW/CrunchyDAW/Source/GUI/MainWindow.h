/*
 * MainWindow.h
 * ------------
 * Description: The main window of the application.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef MAIN_WINDOW_H__DEFINED__
#define MAIN_WINDOW_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/Common/Atomics.h"

#include "Canvas/Canvas.h"
#include "Timeline/Timeline.h"
#include "Library/Library.h"
//#include "AudioControls/AudioControls.h"

#include "GeneratedFiles/ui_MainWindow.h"

#include <QtWidgets/QMainWindow>
#include <QtWidgets/qsplitter.h>

class Application;

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	MainWindow( const Application &app );
	~MainWindow();

	bool initialize();
	void terminate();

	bool event( QEvent *event );

	// This must be called AFTER all audio state is changed (either atomically or with locks)
	// It will queue a UI callback, but only if one doesn't already exist
	void queueAudioEvent();

	// Public sub-UI components
	Library *library;

private:
	Ui::MainWindow ui;
	// Additional UI
	QSplitter *canvasTimelineSplitter;
	QSplitter *libraryCanvasAudioControlsSplitter;

	// GUI has read access to application, but cannot modify it directly
	const Application &app;

	QEvent::Type audioEventType; // Registered from Qt
	AtomicInt32 queuedAudioEventCount;
	void processAudioEvents();

private slots:
	void on_actionOptions_triggered( bool checked );
};

#endif // MAIN_WINDOW_H__DEFINED__
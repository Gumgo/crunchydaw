#include "OptionsDialog.h"

#include "Source/AudioEngine/AudioDevice/AudioDeviceSupport.h"
#include "Source/AudioEngine/AudioDevice/AudioDevice.h"
#include "Source/AudioEngine/AudioDevice/AudioDeviceDirectSound.h"
#include "Source/AudioEngine/AudioDevice/AudioDeviceASIO.h"

OptionsDialog::OptionsDialog( const Application &app, QWidget *parent )
	: app( app )
	, QDialog( parent ) {
	ui.setupUi( this );

	const std::vector<AudioDeviceInfo> &audioDevices = app.audioEngine.getAvailableDevices();
	QComboBox *audioDeviceList = ui.audioDeviceList;
	for (size_t i = 0; i < audioDevices.size(); ++i) {
		QString name = QString::fromWCharArray( audioDevices[i].name.c_str() );
		audioDeviceList->addItem( name );
	}
	audioDeviceList->setCurrentIndex( app.audioEngine.getCurrentAudioDeviceIndex() );
}

OptionsDialog::~OptionsDialog() {
}

void OptionsDialog::on_okButton_clicked() {
	// $TODO Save settings
	close();
}

void OptionsDialog::on_cancelButton_clicked() {
	// $TODO Close and don't save settings
	close();
}

void OptionsDialog::on_applyButton_clicked() {
	// $TODO Save settings don't close
}
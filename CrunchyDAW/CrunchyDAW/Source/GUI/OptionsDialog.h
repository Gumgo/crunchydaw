/*
 * OptionsDialog.h
 * ---------------
 * Description: A dialog containing program options.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef OPTIONS_DIALOG_H__DEFINED__
#define OPTIONS_DIALOG_H__DEFINED__

#include "Source/Common/Common.h"

#include "Source/Application.h"

#include "GeneratedFiles/ui_OptionsDialog.h"

#include <qdialog.h>

class OptionsDialog : public QDialog {
	Q_OBJECT

public:
	OptionsDialog( const Application &app, QWidget *parent = 0 );
	~OptionsDialog();

private:
	Ui::OptionsDialog ui;
	const Application &app;

private slots:
	void on_okButton_clicked();
	void on_cancelButton_clicked();
	void on_applyButton_clicked();
};

#endif // OPTIONS_DIALOG_H__DEFINED__
/*
 * Canvas.h
 * --------
 * Description: The canvas on which modules are arranged.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef CANVAS_H__DEFINED__
#define CANVAS_H__DEFINED__

#include "Source/Common/Common.h"

#include <qgraphicsview.h>
#include <qgraphicsitem.h>

class Application;

class ModuleItem : public QGraphicsItem {
public:
private:
};

class ConnectionItem : public QGraphicsItem {
public:
private:
};

class Canvas : public QGraphicsView {
	Q_OBJECT

public:
	Canvas( const Application &app, QWidget *parent = 0 );

private:
	const Application &app;
};

#endif // CANVAS_H__DEFINED__
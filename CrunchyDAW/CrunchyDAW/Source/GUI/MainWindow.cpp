#include "MainWindow.h"

#include "OptionsDialog.h"
#include "Source/Application.h"

#include <qmessagebox.h>

// $TEMP
#include <qtextedit.h>
#include <qgraphicsview.h>

MainWindow::MainWindow( const Application &app )
	: app( app )
	, QMainWindow( NULL ) {
	// $TODO let's just make all the UI programmatic, since it seems clunky to have it divided
	ui.setupUi( this );

	// Set up additional UI
	canvasTimelineSplitter = new QSplitter( Qt::Vertical );
	libraryCanvasAudioControlsSplitter = new QSplitter( Qt::Horizontal );
	QGraphicsView *canvas = new QGraphicsView();
	QTextEdit *timeline = new QTextEdit();
	library = new Library( app );
	QTextEdit *audioControls = new QTextEdit();

	timeline->setText( "Timeline" );
	audioControls->setText( "AudioControls" );

	ui.windowCanvas->setLayout( new QHBoxLayout() );
	ui.windowCanvas->layout()->addWidget( canvasTimelineSplitter );

	canvasTimelineSplitter->addWidget( libraryCanvasAudioControlsSplitter );
	canvasTimelineSplitter->addWidget( timeline );

	libraryCanvasAudioControlsSplitter->addWidget( library );
	libraryCanvasAudioControlsSplitter->addWidget( canvas );
	libraryCanvasAudioControlsSplitter->addWidget( audioControls );

	// Is this really how custom events are registered, with this ugly cast and assigning an invalid value to an enum!?
	audioEventType = static_cast<QEvent::Type>( QEvent::registerEventType() );
	queuedAudioEventCount.exchange( 0 );

	// Check whether we have a valid audio device
	if (!app.audioEngine.getCurrentAudioDevice()) {
		QMessageBox alert( QMessageBox::Information, "Audio Device",
			"No audio device opened. Select an audio device from the options dialog.",
			QMessageBox::Ok );
		alert.exec();
	}
}

MainWindow::~MainWindow() {
	terminate();
}

bool MainWindow::initialize() {
	bool result = library->initialize();
	return result;
}

void MainWindow::terminate() {
	library->terminate();
}

bool MainWindow::event( QEvent *event ) {
	if (event->type() == audioEventType) {
		processAudioEvents();
		return true;
	} else {
		return QMainWindow::event( event );
	}
}

void MainWindow::queueAudioEvent() {
	// Audio state should be changed BEFORE this is called

	int32 oldCount = queuedAudioEventCount.increment();
	if (oldCount == 0) {
		// There were no UI events queued previously, so add one now
		QApplication::postEvent( this, new QEvent( audioEventType ) );
	}
}

void MainWindow::processAudioEvents() {
	// We need to make sure we don't miss any audio events. Therefore, we atomically set the queuedAudioEventCount to 0
	// BEFORE we actually process the new audio data. If we were to set it after, the following could happen:
	// - We process data that has been previously queued
	// - Before we reset the queuedAudioEventCount to 0, another thread tries to queue another event
	// - That thread atomically increments the queuedAudioEventCount, but since it was NOT previously 0, it does not
	//   actually queue an event to process the new data (since it expects that one is already queued)
	// - processAudioEvents() finishes by setting queuedAudioEventCount to 0, and the new data is missed until another
	//   event is queued later
	// By incrementing queuedAudioEventCount before processing the data, the worst that can happen is:
	// - We reset queuedAudioEventCount to 0
	// - Another thread queues an audio event, and since queuedAudioEventCount is 0, another event to process the data
	//   is queued
	// - We process the data
	// - We process the data redundantly a second time, since another event was queued
	// ... Which isn't really a problem, as long as our desired behavior is to always see the FINAL audio state, rather
	// that to see each state change individually
	queuedAudioEventCount.exchange( 0 );

	// Process updated audio state here:
	// Note: audio state should be read atomically or using locks

	// $TODO
}

void MainWindow::on_actionOptions_triggered( bool checked ) {
	OptionsDialog *optionsDialog = new OptionsDialog( app, this );
	optionsDialog->exec();
	delete optionsDialog;
}
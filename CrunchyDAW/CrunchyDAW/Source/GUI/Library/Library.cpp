#include "Library.h"

#include "Source/Application.h"

#include <qboxlayout.h>
#include <qevent.h>

// Built-in modules:
#include "Source/Modules/Library/WaveGeneratorModule.h"

// Overrides keyPressEvent() to allow ESC to clear the text
class LineEditWithEscape : public QLineEdit {
public:
	LineEditWithEscape( QWidget *parent = 0 )
		: QLineEdit( parent ) {
	}

protected:
	void keyPressEvent( QKeyEvent *event ) {
		if (event->key() == Qt::Key_Escape) {
			clear();
		} else {
			QLineEdit::keyPressEvent( event );
		}
	}
};

bool searchMatches( const std::wstring &searchFilter, const std::wstring &str );

Library::Library( const Application &app, QWidget *parent )
	: QWidget( parent )
	, app( app ) {
	// Set up the UI
	searchEdit = new LineEditWithEscape();
	refreshButton = new QPushButton();
	libraryTree = new QTreeWidget();
	libraryTree->setHeaderHidden( true );

	QHBoxLayout *searchLayout = new QHBoxLayout();
	searchLayout->addWidget( searchEdit );
	searchLayout->addWidget( refreshButton );

	QVBoxLayout *libraryLayout = new QVBoxLayout();
	libraryLayout->addLayout( searchLayout );
	libraryLayout->addWidget( libraryTree );

	libraryLayout->setContentsMargins( 0, 0, 0, 0 );
	setLayout( libraryLayout );

	connect( searchEdit, &QLineEdit::textChanged, this, &Library::searchTextChanged );
	connect( refreshButton, &QPushButton::clicked, this, &Library::refresh );

	// Add double-click handler to the tree
	connect( libraryTree, &QTreeWidget::itemDoubleClicked, this, &Library::doubleClickedTreeItem );
}

bool Library::initialize() {
	refresh();
	return true;
}

void Library::terminate() {
}

void Library::refresh() {
	libraryModules.clear();

	{ // WaveGeneratorModule
		LibraryModule lm;
		lm.moduleUuid	= app.moduleRegistry.getModuleUuid( WaveGeneratorModuleData::NAME );
		lm.category		= LibraryModule::CATEGORY_BUILT_IN;
		lm.displayName	= L"Wave Generator";
		libraryModules.push_back( lm );
	}

	// $TODO scan VST folder

	// Sort the registered modules alphabetically
	struct ModuleNameSort {
		bool operator()( const LibraryModule &mA, const LibraryModule &mB ) const {
			return mA.displayName < mB.displayName;
		}
	};
	std::sort( libraryModules.begin(), libraryModules.end(), ModuleNameSort() );

	updateLibraryTree( false );
}

void Library::updateLibraryTree( bool keepFoldersAndSelection ) {
	static const wchar_t *CATEGORY_STRINGS[LibraryModule::CATEGORY_COUNT] = {
		L"Built In",
		L"VSTs"
	};

	// Keep track of which folders were open and item selection
	int selectedItemType = INDEX_NONE<int>::value;
	size_t selectedItemIndex = INDEX_NONE<size_t>::value;
	bool categoriesExpanded[LibraryModule::CATEGORY_COUNT];
	std::fill( categoriesExpanded, categoriesExpanded + LibraryModule::CATEGORY_COUNT, false );

	if (keepFoldersAndSelection) {
		BOOST_ASSERT( libraryTree->topLevelItemCount() == LibraryModule::CATEGORY_COUNT );

		// Store which categories are expanded
		for (size_t i = 0; i < LibraryModule::CATEGORY_COUNT; ++i) {
			categoriesExpanded[i] = libraryTree->topLevelItem( static_cast<int>( i ) )->isExpanded();
		}

		QTreeWidgetItem *item = libraryTree->currentItem();
		if (item) {
			selectedItemType = item->type();
			if (selectedItemType == LIBRARY_TREE_TYPE_MODULE) {
				selectedItemIndex = item->data( 0, Qt::UserRole ).toUInt();
				BOOST_ASSERT( selectedItemIndex < libraryModules.size() );
			}
		}
	}

	libraryTree->clear();

	// Add folders for categories
	for (size_t i = 0; i < LibraryModule::CATEGORY_COUNT; ++i) {
		QTreeWidgetItem *category = new QTreeWidgetItem( LIBRARY_TREE_TYPE_CATEGORY );
		category->setText( 0, QString::fromWCharArray( CATEGORY_STRINGS[i] ) );
		libraryTree->insertTopLevelItem( i, category );

		if (categoriesExpanded[i]) {
			category->setExpanded( true );
		}
		if (selectedItemType == LIBRARY_TREE_TYPE_CATEGORY && selectedItemIndex == i) {
			libraryTree->setCurrentItem( category );
		}
	}

	std::wstring searchText = searchEdit->text().toStdWString();
	bool filter = !searchText.empty();

	// Add each module to the appropriate folder
	for (size_t i = 0; i < libraryModules.size(); ++i) {
		const LibraryModule &lm = libraryModules[i];
		BOOST_ASSERT( lm.category < LibraryModule::CATEGORY_COUNT );

		// If the search string is non-empty, filter the search results
		if (filter && !searchMatches( searchText, lm.displayName )) {
			continue;
		}

		QTreeWidgetItem *module = new QTreeWidgetItem( LIBRARY_TREE_TYPE_MODULE );
		module->setText( 0, QString::fromWCharArray( lm.displayName.c_str() ) );
		module->setData( 0, Qt::UserRole, QVariant( static_cast<uint32>( i ) ) );
		QTreeWidgetItem *category = libraryTree->topLevelItem( static_cast<int>( lm.category ) );
		category->insertChild( category->childCount(), module );

		if (selectedItemType == LIBRARY_TREE_TYPE_MODULE && selectedItemIndex == i) {
			libraryTree->setCurrentItem( module );
		}
	}
}

void Library::searchTextChanged( const QString &text ) {
	// Note: Could probably do this more efficiently instead of rebuilding the whole tree, but this is fine for now
	updateLibraryTree( true );
}

void Library::doubleClickedTreeItem( QTreeWidgetItem *item, int column ) {
	if (!item) {
		return;
	}

	if (item->type() != LIBRARY_TREE_TYPE_MODULE) {
		return;
	}

	uint32 selectedItemIndex = item->data( 0, Qt::UserRole ).toUInt();
	BOOST_ASSERT( selectedItemIndex < libraryModules.size() );

	// Call the behavior function with the module UUID
	addModuleToProjectBehavior( libraryModules[selectedItemIndex].moduleUuid );
}

bool searchMatches( const std::wstring &searchFilter, const std::wstring &str ) {
	if (str.length() < searchFilter.length()) {
		// Filter has too many characters, can't match
		return false;
	}

	size_t cmpCount = str.length() - searchFilter.length();
	for (size_t i = 0; i < cmpCount; ++i) {
		// Compare each character
		bool equal = true;
		for (size_t c = 0; equal && c < searchFilter.length(); ++c) {
			wchar_t charA = tolower( searchFilter[c] );
			wchar_t charB = tolower( str[c + i] );
			equal = (charA == charB);
		}
		if (equal) {
			// Filter matched part of the string
			return true;
		}
	}

	return false;
}
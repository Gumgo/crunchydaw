/*
 * Library.h
 * ---------
 * Description: The canvas on which modules are arranged.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef LIBRARY_H__DEFINED__
#define LIBRARY_H__DEFINED__

#include "Source/Common/Common.h"

#include <qlineedit.h>
#include <qpushbutton.h>
#include <qtreewidget.h>

#include <vector>
#include <boost/uuid/uuid.hpp>

class Application;
class Module;

class Library : public QWidget {
	Q_OBJECT

public:
	Library( const Application &app, QWidget *parent = 0 );

	bool initialize();
	void terminate();

	// Behavior function pointers

	// void addModuleToProjectBehavior( const boost::uuids::uuid &moduleUuid )
	fastdelegate::FastDelegate1<const boost::uuids::uuid &> addModuleToProjectBehavior;

private:
	const Application &app;
	QLineEdit *searchEdit;
	QPushButton *refreshButton;
	QTreeWidget *libraryTree;

	static const int LIBRARY_TREE_TYPE_CATEGORY = 0;
	static const int LIBRARY_TREE_TYPE_MODULE = 1;

	// Re-registers modules and scans plugin folders
	void refresh();
	// Called when tree is refreshed or when search string changes
	void updateLibraryTree( bool keepFoldersAndSelection );

	// Called when text of the search bar is changed
	void searchTextChanged( const QString &text );

	// Called when the library tree is double clicked
	void doubleClickedTreeItem( QTreeWidgetItem *item, int column );

	struct LibraryModule {
		enum Category {
			CATEGORY_BUILT_IN,
			CATEGORY_VST,

			CATEGORY_COUNT
		};

		boost::uuids::uuid moduleUuid;
		Category category;
		std::wstring displayName;
		// $TODO other data
	};

	std::vector<LibraryModule> libraryModules;
};

#endif // LIBRARY_H__DEFINED__
/*
 * Platform.h
 * ----------
 * Description: Provides compile-time defines to detect the platform.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef PLATFORM_H__DEFINED__
#define PLATFORM_H__DEFINED__

#define PLATFORM_WINDOWS	0
#define PLATFORM_APPLE		1
#define PLATFORM_LINUX		2

#if defined(_WIN32)
#define CURRENT_PLATFORM	PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define CURRENT_PLATFORM	PLATFORM_APPLE
#elif defined(__linux__)
#define CURRENT_PLATFORM	PLATFORM_LINUX
#else // !WIN32 && !__APPLE__ && !__linux__
#error Unsupported platform!
#endif // !WIN32 && !__APPLE__ && !__linux__

#if CURRENT_PLATFORM == PLATFORM_WINDOWS
#define NOMINMAX
#include <Windows.h>
#include <qt_windows.h>
#endif // CURRENT_PLATFORM == PLATFORM_WINDOWS

#endif // PLATFORM_H__DEFINED__
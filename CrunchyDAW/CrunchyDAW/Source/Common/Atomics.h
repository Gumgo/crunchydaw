/*
 * Atomics.h
 * ---------
 * Description: Provides atomic types.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef ATOMICS_H__DEFINED__
#define ATOMICS_H__DEFINED__

#include "Types.h"
#include "Platform.h"

class AtomicInt32 {
public:
	// Initializes to 0
	AtomicInt32();
	AtomicInt32( int32 value );

	// These functions return old values
	int32 add( int32 value );
	int32 increment();
	int32 decrement();
	int32 exchange( int32 value );

	// Exchanges current value for value if current value equals comparison
	int32 compareExchange( int32 value, int32 comparison );

	// After calling this, the value returned expires immediately
	int32 get();

private:
#if CURRENT_PLATFORM == PLATFORM_WINDOWS
	volatile LONG value;
	BOOST_STATIC_ASSERT( sizeof( LONG ) == sizeof( int32 ) );
#elif CURRENT_PLATFORM == PLATFORM_APPLE
#error Not yet implemented!
#elif CURRENT_PLATFORM == PLATFORM_LINUX
#error Not yet implemented!
#else // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
#error Unsupported platform!
#endif // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
};

#endif // ATOMICS_H__DEFINED__
/*
 * UuidGenerator.h
 * ---------------
 * Description: Provides a mechanism for generating UUIDs to uniquely identify objects.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef UUID_GENERATOR_H__DEFINED__
#define UUID_GENERATOR_H__DEFINED__

#include "Common.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/unordered_set.hpp>

class UuidGenerator {
public:
	// Generates a new random UUID
	boost::uuids::uuid generateUuid();

	// Attempts to allocate the given UUID if it isn't in use
	bool allocateUuid( const boost::uuids::uuid &uuid );

	// Frees the given UUID if it is in use
	bool freeUuid( const boost::uuids::uuid &uuid );

	// Queries whether the given UUID is in use
	bool isUuidInUse( const boost::uuids::uuid &uuid ) const;

	// Clears state of in-use UUIDs
	void clear();

private:
	// Internal UUID generator
	boost::uuids::basic_random_generator<boost::mt19937> generator;

	// Set of UUIDs in use
	typedef boost::unordered_set<boost::uuids::uuid> UuidSet;
	UuidSet uuids;
};

#endif // UUID_GENERATOR_H__DEFINED__
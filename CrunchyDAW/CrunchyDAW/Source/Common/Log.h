/*
 * Log.h
 * -----
 * Description: Provides ability to write to a log.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef LOG_H__DEFINED__
#define LOG_H__DEFINED__

#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <boost/thread.hpp>
#include <boost/noncopyable.hpp>

class Log;

// Returns application
// Returns the application log, asserting that it is set
Log &appLog();

// The log is a utility which allows messages to be written to a file using the << operator. The log is thread safe and
// messages chained together using << are atomic. Each message logged has a logging level and a category. Logging level
// defines the severity of the message, e.g. "verbose" is for debug info and "error" is for errors. Category is used to
// express where the message comes from. Categories are of type const char * and take the following form:
// "category/sub_category123/anotherSubCategory". (There can be as many subcategories as desired.) Any category strings
// used should be static - undefined behavior will result otherwise (probably crashes!). Messages can be filtered by
// logging level and by category. Category filtering gets "stronger" with each subcategory. For example, if the category
// "audio" is at level "warning" and the category "audio/driver" is at level "verbose", any message with at least the
// category "audio/driver" will get shown up to level "verbose".
class Log : public boost::noncopyable {
public:
	// If isApplicationLog is true, this log is accessible through the static log pointer
	// Only one log at a time is allowed to have this status; however, multiple non-static logs may exist
	// To avoid a lock when accessing the log each time, the application log should be created at the very start of the
	// application, before threading issues can arise
	Log( bool isApplicationLog );
	~Log();

	bool open( const std::string &outFname );
	bool flush();
	bool close();
	bool isOpen() const;

	// Level of logging
	enum LoggingLevel {
		LOGGING_LEVEL_VERBOSE = 0,
		LOGGING_LEVEL_MESSAGE,
		LOGGING_LEVEL_WARNING,
		LOGGING_LEVEL_ERROR,
		LOGGING_LEVEL_CRITICAL,

		LOGGING_LEVEL_COUNT,

		LOGGING_LEVEL_DEFAULT_FILTER = LOGGING_LEVEL_MESSAGE
	};

	void setLoggingLevelFilter( LoggingLevel minLevel );
	LoggingLevel getLoggingLevelFilter() const;

	void setLoggingLevelFilterForCategory( const char *category, LoggingLevel minLevel );
	LoggingLevel getLoggingLevelFilterForCategory( const char *category ) const;

private:
	// Application log pointer
	static Log *applicationLog;
	bool isApplicationLog;
	friend Log &appLog();

	// Represents an atomic message
	// The message is written to a local stream, and upon destruction the lock is acquired, the local stream is written
	// to the log, and then the lock is released
	// Since Message is noncopyable and private it cannot be stored on the stack, only used "immediately" and disposed
	class Message : private boost::noncopyable {
	public:
		Message( Log &log, LoggingLevel level, const char *category );

		// Acquires lock, writes to log output, releases lock
		~Message();

		// Allows writing to message using << operator; returns *this to allow chaining
		template<typename T> Message &operator<<( const T & t );

	private:

		// Reference to underlying log class
		Log &log;

		// The local stream for this message
		std::wstringstream localStream;

		// Whether this message should be ignored
		bool ignore;
	};

	std::wofstream outStream;

	// Make sure multiple threads don't collide
	boost::mutex logLock;

	// The minimum logging level, unless overridden using a category-specific filter
	LoggingLevel minLoggingLevel;

	// Stores explicit length
	// This is so that we can easily deal with subcategories by setting length, rather than relying on \0
	struct Category {
		const char *category;
		size_t length;

		bool operator<( const Category &o ) const;
	};

	// Min logging levels for categories
	typedef std::map<Category, LoggingLevel> CategoryMap;
	CategoryMap categoryMinLoggingLevels;

	LoggingLevel getMinLoggingLevelForCategory( const char *category ) const;

public:
	// Logging functions
	// Example usage: warning("audio:driver") << "Something went wrong!";
	Message verbose( const char *category );
	Message message( const char *category );
	Message warning( const char *category );
	Message error( const char *category );
	Message critical( const char *category );
	// Version taking enum
	Message log( LoggingLevel loggingLevel, const char *category );
};

template<typename T> Log::Message &Log::Message::operator<<( const T & t ) {
	if (!ignore) {
		localStream << t;
	}
	return *this;
}

// It seems that std::wstringstream can't directly take std::string so define this operator separately
inline std::wostream &operator<<( std::wostream &ostr, const std::string &str ) {
	// Calling c_str() technically invalidates any iterators pointing to the string...
	// It also wouldn't deal with embedded \0 (not that we'd ever use that)
	// Do this instead
	std::copy( str.begin(), str.end(), std::ostream_iterator<char, wchar_t>( ostr ) );
	return ostr;
}

#endif // LOG_H__DEFINED__
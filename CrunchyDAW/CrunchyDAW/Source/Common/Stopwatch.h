/*
 * Stopwatch.h
 * -----------
 * Description: A class for measuring accurate timing.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef STOPWATCH_H__DEFINED__
#define STOPWATCH_H__DEFINED__

#include "Types.h"
#include "Platform.h"

class Stopwatch {
public:
	Stopwatch();

	void start();
	void stop();

	int64 getStartTimeMicros() const;
	int64 getStopTimeMicros() const;
	int64 getDurationMicros() const;
	static int64 getCurrentTimeMicros();

	int64 getStartTimeMillis() const;
	int64 getStopTimeMillis() const;
	int64 getDurationMillis() const;
	static int64 getCurrentTimeMillis();

private:
#if CURRENT_PLATFORM == PLATFORM_WINDOWS
	LARGE_INTEGER frequency;
	LARGE_INTEGER startTime;
	LARGE_INTEGER stopTime;
#elif CURRENT_PLATFORM == PLATFORM_APPLE
#error Not yet implemented
#elif CURRENT_PLATFORM == PLATFORM_LINUX
#error Not yet implemented
#else // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
#error Platform not supported
#endif // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
};

#endif // STOPWATCH_H__DEFINED__
#include "Stopwatch.h"

// Non-platform-specific functions:

int64 Stopwatch::getCurrentTimeMicros() {
	Stopwatch sw;
	sw.start();
	return sw.getStartTimeMicros();
}

int64 Stopwatch::getCurrentTimeMillis() {
	Stopwatch sw;
	sw.start();
	return sw.getStartTimeMillis();
}

#if CURRENT_PLATFORM == PLATFORM_WINDOWS

int64 toMicros( int64 time, int64 freq ) {
	return (time * 1000000) / freq;
}

int64 toMillis( int64 time, int64 freq ) {
	return (time * 1000) / freq;
}

Stopwatch::Stopwatch() {
	frequency.QuadPart = 0;
	startTime.QuadPart = 0;
	stopTime.QuadPart = 0;
}

void Stopwatch::start() {
	QueryPerformanceFrequency( &frequency );
	QueryPerformanceCounter( &startTime );
}

void Stopwatch::stop() {
	QueryPerformanceCounter( &stopTime );
}

int64 Stopwatch::getStartTimeMicros() const {
	return toMicros( startTime.QuadPart, frequency.QuadPart );
}

int64 Stopwatch::getStopTimeMicros() const {
	return toMicros( stopTime.QuadPart, frequency.QuadPart );
}

int64 Stopwatch::getDurationMicros() const {
	if (stopTime.QuadPart < startTime.QuadPart) {
		return 0;
	} else {
		return toMicros( stopTime.QuadPart - startTime.QuadPart, frequency.QuadPart );
	}
}

int64 Stopwatch::getStartTimeMillis() const {
	return toMillis( startTime.QuadPart, frequency.QuadPart );
}

int64 Stopwatch::getStopTimeMillis() const {
	return toMillis( stopTime.QuadPart, frequency.QuadPart );
}

int64 Stopwatch::getDurationMillis() const {
	if (stopTime.QuadPart < startTime.QuadPart) {
		return 0;
	} else {
		return toMillis( stopTime.QuadPart - startTime.QuadPart, frequency.QuadPart );
	}
}

#elif CURRENT_PLATFORM == PLATFORM_APPLE
#error Not yet implemented
#elif CURRENT_PLATFORM == PLATFORM_LINUX
#error Not yet implemented
#else // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
#error Platform not supported
#endif // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
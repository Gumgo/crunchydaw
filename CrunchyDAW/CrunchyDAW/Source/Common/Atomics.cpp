#include "Atomics.h"

#if CURRENT_PLATFORM == PLATFORM_WINDOWS

AtomicInt32::AtomicInt32() {
	exchange( 0 );
}

AtomicInt32::AtomicInt32( int32 value ) {
	exchange( value );
}

int32 AtomicInt32::add( int32 value ) {
	return static_cast<int32>( InterlockedExchangeAdd( &this->value,
		static_cast<LONG>( value ) ) );
}

int32 AtomicInt32::increment() {
	return static_cast<int32>( InterlockedIncrement( &this->value ) );
}

int32 AtomicInt32::decrement() {
	return static_cast<int32>( InterlockedDecrement( &this->value ) );
}

int32 AtomicInt32::exchange( int32 value ) {
	return static_cast<int32>( InterlockedExchange( &this->value,
		static_cast<LONG>( value ) ) );
}

int32 AtomicInt32::compareExchange( int32 value, int32 comparison ) {
	return static_cast<int32>( InterlockedCompareExchange( &this->value,
		static_cast<LONG>( value ), static_cast<LONG>( value ) ) );
}

int32 AtomicInt32::get() {
	// Do this so we get the memory barriers
	// Since we're using compare/exchange, we pick a value that probably won't be hit often to compare against, and
	// exchange it with itself. This is so we usually avoid writing.
	return static_cast<int32>( InterlockedCompareExchange( &this->value, 0xabababab, 0xabababab ) );
}

#elif CURRENT_PLATFORM == PLATFORM_APPLE
#error Not yet implemented!
#elif CURRENT_PLATFORM == PLATFORM_LINUX
#error Not yet implemented!
#else // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
#error Unsupported platform!
#endif // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
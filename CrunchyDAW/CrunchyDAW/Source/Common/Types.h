/*
 * Types.h
 * -------
 * Description: Defines standard set of types.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef TYPES_H__DEFINED__
#define TYPES_H__DEFINED__

#include "Asserts.h"

typedef unsigned char		uint8;
typedef char				int8;
typedef unsigned short		uint16;
typedef short				int16;
typedef unsigned int		uint32;
typedef int					int32;
typedef unsigned long long	uint64;
typedef long long			int64;

BOOST_STATIC_ASSERT( sizeof( uint8 ) == 1 );
BOOST_STATIC_ASSERT( sizeof( int8 ) == 1 );
BOOST_STATIC_ASSERT( sizeof( uint16 ) == 2 );
BOOST_STATIC_ASSERT( sizeof( int16 ) == 2 );
BOOST_STATIC_ASSERT( sizeof( uint32 ) == 4 );
BOOST_STATIC_ASSERT( sizeof( int32 ) == 4 );
BOOST_STATIC_ASSERT( sizeof( uint64 ) == 8 );
BOOST_STATIC_ASSERT( sizeof( int64 ) == 8 );

template<typename T>
struct INDEX_NONE {
	static const T value = (T)-1;
};

#endif // TYPES_H__DEFINED__
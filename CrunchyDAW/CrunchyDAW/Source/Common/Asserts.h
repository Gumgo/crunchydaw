/*
 * Asserts.h
 * ---------
 * Description: Provides a set of assert functions.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef ASSERTS_H__DEFINED__
#define ASSERTS_H__DEFINED__

// use boost's assert and static assert libraries
#include <boost/assert.hpp>
#include <boost/static_assert.hpp>

#ifdef BOOST_ASSERT
#define ASSERTS_ENABLED 1
#else // BOOST_ASSERT
#define ASSERTS_ENABLED 0
#endif // BOOST_ASSERT

#endif // ASSERTS_H__DEFINED__
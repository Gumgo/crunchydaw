#include "Log.h"

#include "Asserts.h"
#include <boost/date_time.hpp>

// Category asserts involve looping over strings, which could be a bit expensive, so we can disable them if desired
#define CATEGORY_ASSERTS_ENABLED 1

Log *Log::applicationLog = NULL;

Log &appLog() {
	BOOST_ASSERT( Log::applicationLog );
	return *Log::applicationLog;
}

// Strings for each logging level - make sure they're the same length for alignment
static const char *LOGGING_LEVEL_STRINGS[Log::LOGGING_LEVEL_COUNT] = {
	"Verbose   ",
	"Message   ",
	"Warning   ",
	"Error     ",
	"Critical  "
};

static void assertCategoryIsValid( const char *category ) {
	// Categories should be of the form
	// blah/blah2/blah3
	// with forward slashes in between each subcategory, and the valid characters:
	// a-z, A-Z, 0-9, _
	// no leading or trailing slashes

#if (ASSERTS_ENABLED == 1) && (CATEGORY_ASSERTS_ENABLED == 1)
	bool done = false;
	bool atLeastOneCharacter = false;
	while (!done) {
		char ch = *category;
		bool end = false;
		if ((ch >= 'a' && ch <= 'z') ||
			(ch >= 'A' && ch <= 'Z') ||
			(ch >= '0' && ch <= '9') ||
			ch == '_') {
			atLeastOneCharacter = true;
		} else if (ch == '/' || ch == '\0') {
			end = true;
		} else {
			BOOST_ASSERT_MSG( false, "Category contains invalid character" );
		}

		if (end) {
			BOOST_ASSERT_MSG( atLeastOneCharacter, "Category is empty or contains empty subcategory" );
			atLeastOneCharacter = false;
			if (ch == '\0') {
				done = true;
			} else {
				BOOST_ASSERT( ch == '/' );
				++category;
			}
		} else {
			atLeastOneCharacter = true;
			++category;
		}
	}
#endif // (ASSERTS_ENABLED == 1) && (CATEGORY_ASSERTS_ENABLED == 1)
}

Log::Log( bool isApplicationLog )
	: isApplicationLog( isApplicationLog ) {
	if (isApplicationLog) {
		// Make sure no other application log exists
		BOOST_ASSERT( !applicationLog );
		applicationLog = this;
	}

	setLoggingLevelFilter( LOGGING_LEVEL_DEFAULT_FILTER );
}

Log::~Log() {
	close();

	if (isApplicationLog) {
		applicationLog = NULL;
	}
}

bool Log::open( const std::string &outFname ) {
	if (isOpen()) {
		return false;
	}

	outStream = std::wofstream( outFname );
	return isOpen();
}

bool Log::flush() {
	if (isOpen()) {
		outStream.flush();
		return true;
	} else {
		return false;
	}
}

bool Log::close() {
	if (isOpen()) {
		flush();
		outStream.close();
		return true;
	} else {
		return false;
	}
}

bool Log::isOpen() const {
	return outStream.is_open();
}

void Log::setLoggingLevelFilter( LoggingLevel minLevel ) {
	BOOST_ASSERT( minLevel < LOGGING_LEVEL_COUNT );
	minLoggingLevel = minLevel;
}

Log::LoggingLevel Log::getLoggingLevelFilter() const {
	return minLoggingLevel;
}

void Log::setLoggingLevelFilterForCategory( const char *category, LoggingLevel minLevel ) {
	BOOST_ASSERT( minLevel < LOGGING_LEVEL_COUNT );
	assertCategoryIsValid( category );

	// Put into map - overwrite any previous entry
	Category c;
	c.category = category;
	c.length = strlen( category );
	categoryMinLoggingLevels.insert( std::make_pair( c, minLevel ) );
}

Log::LoggingLevel Log::getLoggingLevelFilterForCategory( const char *category ) const {
	assertCategoryIsValid( category );

	Category c;
	c.category = category;
	c.length = strlen( category );
	CategoryMap::const_iterator it = categoryMinLoggingLevels.find( c );
	if (it == categoryMinLoggingLevels.end()) {
		// Return the default if category is not filtered
		return minLoggingLevel;
	} else {
		return it->second;
	}
}

bool Log::Category::operator<( const Category &o ) const {
	// Compare lengths first
	if (length < o.length) {
		return true;
	} else if (o.length < length) {
		return false;
	}

	// Compare each character, return if they differ
	for (size_t i = 0; i < length && i < o.length; ++i) {
		if (category[i] < o.category[i]) {
			return true;
		} else if (o.category[i] < category[i]) {
			return false;
		}
	}

	// They are equal
	return false;
}

Log::LoggingLevel Log::getMinLoggingLevelForCategory( const char *category ) const {
	assertCategoryIsValid( category );

	// Walk the subcategories, taking the min logging level each time
	// If we don't find any entry for any subcategory, return the default logging level
	LoggingLevel currentMin = LOGGING_LEVEL_COUNT;
	Category c;
	c.category = category;
	c.length = 0;
	bool done = false;
	while (!done) {
		char ch = c.category[c.length];
		if (ch == '/' || ch == '\0') {
			// End of subcategory, test filter
			CategoryMap::const_iterator it = categoryMinLoggingLevels.find( c );
			if (it != categoryMinLoggingLevels.end()) {
				// Take min
				currentMin = std::min( currentMin, it->second );
			}
		}

		if (ch == '\0') {
			done = true;
		} else {
			++c.length;
		}
	}

	if (currentMin == LOGGING_LEVEL_COUNT) {
		currentMin = getLoggingLevelFilter();
	}

	return currentMin;
}

Log::Message::Message( Log &log, LoggingLevel level, const char *category )
	: log( log ) {
	// Guard against user error
	BOOST_ASSERT( level < LOGGING_LEVEL_COUNT );
	assertCategoryIsValid( category );

	// Determine whether this message should be ignored
	ignore = (level < log.getMinLoggingLevelForCategory( category ));

	if (!ignore) {
		// Write out the message header:
		// [mm/dd/yyyy hh:mm:ss.ssssss] Level     category/category: Message text here
		// Construct facets with a nonzero ref count constructor so they don't get auto-destroyed
		boost::gregorian::wdate_facet dateFormat( 1 );
		boost::posix_time::wtime_facet timeFormat( 1 );
		dateFormat.format( L"%m/%d/%Y" );
		timeFormat.format( L"%H:%M:%s" );

		// Scope to make sure headerStream is destroyed first, because it decreases refcount of facets
		{
			// Create a separate header stream so we don't mess with the local stream's locale... probably unnecessary
			std::wstringstream headerStream;
			headerStream.imbue( std::locale( headerStream.getloc(), &dateFormat ) );
			headerStream.imbue( std::locale( headerStream.getloc(), &timeFormat ) );

			boost::posix_time::ptime localTime = boost::posix_time::microsec_clock::local_time();
			headerStream << '[' << localTime.date() << ' ' << localTime.time_of_day() << "] " <<
				LOGGING_LEVEL_STRINGS[level] << category << ": ";

			// Write header, message will follow
			localStream << headerStream.rdbuf();
		}
	}
}

Log::Message::~Message() {
	if (!ignore) {
		boost::mutex::scoped_lock( log.logLock );
		// Output contents of the local stream
		log.outStream << localStream.rdbuf() << std::endl;
	}
}

// Logging functions

Log::Message Log::verbose( const char *category ) {
	return log( LOGGING_LEVEL_VERBOSE, category );
}

Log::Message Log::message( const char *category ) {
	return log( LOGGING_LEVEL_MESSAGE, category );
}

Log::Message Log::warning( const char *category ) {
	return log( LOGGING_LEVEL_WARNING, category );
}

Log::Message Log::error( const char *category ) {
	return log( LOGGING_LEVEL_ERROR, category );
}

Log::Message Log::critical( const char *category ) {
	return log( LOGGING_LEVEL_CRITICAL, category );
}

Log::Message Log::log( LoggingLevel loggingLevel, const char *category ) {
	BOOST_ASSERT( loggingLevel < LOGGING_LEVEL_COUNT );
	return Message( *this, loggingLevel, category );
}
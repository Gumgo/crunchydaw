#include "UuidGenerator.h"

boost::uuids::uuid UuidGenerator::generateUuid() {
	boost::uuids::uuid uuid;
	bool inUse;
	do {
		uuid = generator();
		inUse = isUuidInUse( uuid );
	} while (inUse);

	uuids.insert( uuid );
	return uuid;
}

bool UuidGenerator::allocateUuid( const boost::uuids::uuid &uuid ) {
	if (isUuidInUse( uuid )) {
		return false;
	}

	uuids.insert( uuid );
	return true;
}

bool UuidGenerator::freeUuid( const boost::uuids::uuid &uuid ) {
	UuidSet::iterator it = uuids.find( uuid );
	if (it == uuids.end()) {
		return false;
	}

	uuids.erase( it );
	return true;
}

bool UuidGenerator::isUuidInUse( const boost::uuids::uuid &uuid ) const {
	return uuids.find( uuid ) != uuids.end();
}

void UuidGenerator::clear() {
	uuids.clear();
}
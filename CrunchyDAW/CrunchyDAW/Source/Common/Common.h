/*
 * Common.h
 * --------
 * Description: Set of common includes.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef COMMON_H__DEFINED__
#define COMMON_H__DEFINED__

#include "Platform.h"
#include "Asserts.h"
#include "Types.h"
#include "Log.h"
#include "Stopwatch.h"
#include "FastDelegate/FastDelegate.h"
#include "FastDelegate/FastDelegateBind.h"

#include <string>
#include <boost/noncopyable.hpp>

#define ZERO_STRUCT( s ) memset( s, 0, sizeof( *s ) )

#define ARRAY_COUNT( a ) (sizeof( a ) / sizeof( a[0] ))

inline size_t align( size_t size, size_t bits ) {
	size_t mask = (1 << bits) - 1;
	return (size + mask) & ~mask;
}

inline bool isAligned( size_t size, size_t bits ) {
	size_t mask = (1 << bits) - 1;
	return (size & mask) == 0;
}

#endif // COMMON_H__DEFINED__
#include "Source/Common/Common.h"
#include "Source/Application.h"
#include "Source/Behavior/ApplicationBehavior.h"
#include <QtWidgets/QApplication>

int main( int argc, char **argv ) {
	int result;
	{
		// Set up the log before anything else
		// Log will flush and close upon leaving the scope
		Log log( true );
		log.open( "out.log" );
		appLog().message( "application" ) << "Initializing CrunchyDAW...";

		{
			QApplication app( argc, argv );

			Application application;
			ApplicationBehavior behavior( application );

			if (behavior.initialize()) {
				application.mainWindow.show();
				result = app.exec();
				behavior.terminate();
			}
		}

		appLog().message( "application" ) << "Terminating CrunchyDAW with result " << result;
	}

	return result;
}

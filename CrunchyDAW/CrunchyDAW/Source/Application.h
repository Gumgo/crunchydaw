/*
 * Application.h
 * -------------
 * Description: Class used to contain all parts of the application, including data, GUI, and the audio engine.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef APPLICATION_H__DEFINED__
#define APPLICATION_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/Modules/ModuleRegistry.h"
#include "Source/Data/ApplicationData.h"
#include "Source/AudioEngine/AudioEngine.h"
#include "Source/GUI/MainWindow.h"

class Application : private boost::noncopyable {
public:
	Application();

	ModuleRegistry moduleRegistry;
	ApplicationData applicationData;
	AudioEngine audioEngine;
	MainWindow mainWindow;
};

#endif // APPLICATION_H__DEFINED__
/*
 * AudioEngine.h
 * -------------
 * Description: The interface used to interact with the audio engine.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef AUDIO_ENGINE_H__DEFINED__
#define AUDIO_ENGINE_H__DEFINED__

#include "Source/Common/Common.h"
#include "ThreadedEvent/ThreadedEvent.h"
#include "AudioDevice/AudioDeviceManager.h"

class AudioEngine {
public:
	AudioEngine();
	~AudioEngine();

	bool initialize();
	void terminate();

	const std::vector<AudioDeviceInfo> &getAvailableDevices() const;

	AudioDevice *getCurrentAudioDevice();
	const AudioDevice *getCurrentAudioDevice() const;
	size_t getCurrentAudioDeviceIndex() const;

	bool openAudioDevice(
		size_t audioDeviceIndex,
		const AudioDeviceSettings &deviceSettings,
		AudioDevice::AudioEventHandler audioEventHandler,
		AudioDevice::AudioProvider audioProvider );

	bool closeCurrentAudioDevice();

private:
	bool initialized;						// Whether the audio engine is initialized

	ThreadedEvent audioThread;				// Thread which runs audio buffer updates
	AudioDeviceManager audioDeviceManager;	// Manages audio devices

	AudioDevice *currentAudioDevice;		// Currently selected audio device
	size_t currentAudioDeviceIndex;			// Index of currently selected audio device in manager's list

	AudioDevice::AudioEventHandler audioEventHandler;	// Callback to handle audio events

	// Passed to the audio device to provide audio data
	void provideAudio( const AudioDeviceSettings &settings, uint32 frameCount, uint8 *buffer );
};

#endif // AUDIO_ENGINE_H__DEFINED__
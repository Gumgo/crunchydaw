#include "AudioEngine.h"

AudioEngine::AudioEngine() {
	initialized = false;
}

AudioEngine::~AudioEngine() {
	terminate();
}

bool AudioEngine::initialize() {
	BOOST_ASSERT( !initialized );

	currentAudioDevice = NULL;
	currentAudioDeviceIndex = INDEX_NONE<size_t>::value;

	audioDeviceManager.findAvailableDevices();

	initialized = true;
	return true;
}

void AudioEngine::terminate() {
	initialized = false;
}

const std::vector<AudioDeviceInfo> &AudioEngine::getAvailableDevices() const {
	BOOST_ASSERT( initialized );
	return audioDeviceManager.getAvailableDevices();
}

AudioDevice *AudioEngine::getCurrentAudioDevice() {
	return currentAudioDevice;
}

const AudioDevice *AudioEngine::getCurrentAudioDevice() const {
	return currentAudioDevice;
}

size_t AudioEngine::getCurrentAudioDeviceIndex() const {
	return currentAudioDeviceIndex;
}

bool AudioEngine::openAudioDevice(
	size_t audioDeviceIndex,
	const AudioDeviceSettings &deviceSettings,
	AudioDevice::AudioEventHandler audioEventHandler,
	AudioDevice::AudioProvider audioProvider ) {

	// Index needs to be in range of the available devices
	BOOST_ASSERT( initialized );
	BOOST_ASSERT( audioDeviceIndex < audioDeviceManager.getAvailableDevices().size() );

	bool result = true;

	const AudioDeviceInfo &info = audioDeviceManager.getAvailableDevices()[audioDeviceIndex];
	AudioDevice *newAudioDevice = AudioDeviceManager::createAudioDevice( info, audioThread );
	if (!newAudioDevice) {
		appLog().error( "audio/device" ) << "Failed to create audio device '" << info.name << "'";
		result = false;
	}

	if (result) {
		newAudioDevice->setAudioEventHandler( audioEventHandler );
		newAudioDevice->setAudioProvider( audioProvider );

		if (newAudioDevice->open( deviceSettings )) {
			appLog().message( "audio/device" ) << "Opened audio device '" << info.name << "'";
		} else {
			appLog().error( "audio/device" ) << "Failed to open audio device '" << info.name << "'";
			result = false;
		}
	}

	if (!result) {
		if (newAudioDevice) {
			delete newAudioDevice;
		}
	} else {
		currentAudioDevice = newAudioDevice;
		currentAudioDeviceIndex = audioDeviceIndex;

		BOOST_ASSERT( (currentAudioDevice == NULL) == (currentAudioDeviceIndex == INDEX_NONE<size_t>::value) );
	}

	return result;
}

bool AudioEngine::closeCurrentAudioDevice() {
	BOOST_ASSERT( (currentAudioDevice == NULL) == (currentAudioDeviceIndex == INDEX_NONE<size_t>::value) );

	if (currentAudioDevice) {
		bool result = currentAudioDevice->close();
		currentAudioDevice = NULL;
		currentAudioDeviceIndex = INDEX_NONE<size_t>::value;
		return result;
	} else {
		return false;
	}
}

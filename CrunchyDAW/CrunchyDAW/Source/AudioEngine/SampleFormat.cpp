#include "SampleFormat.h"

static const SampleFormatProperties SAMPLE_FORMAT_PROPERTIES[SAMPLE_FORMAT_COUNT] = {
	//	isUnsigned	isInt	isFloat		bitsPerSample	silenceByte
	{	true,		true,	false,		8,				0x80	}, // Unsigned 8-bit silence is 128 = 0x80
	{	false,		true,	false,		16,				0x00	}, // Signed silence is 0 for any integer size
	{	false,		true,	false,		24,				0x00	},
	{	false,		true,	false,		32,				0x00	},
	{	false,		false,	true,		32,				0x00	}  // 0.0f == 0x00000000, so 0x00 works for all bytes
};

static const uint32 SAMPLE_RATES[] = {
	22050,
	44100,
	48000,
	88200,
	96000,
	192000
};

const SampleFormatProperties & getSampleFormatProperties( SampleFormat sampleFormat ) {
	BOOST_ASSERT( sampleFormat < SAMPLE_FORMAT_COUNT );

	return SAMPLE_FORMAT_PROPERTIES[sampleFormat];
}

size_t getSampleRateCount() {
	return sizeof( SAMPLE_RATES ) / sizeof( SAMPLE_RATES[0] );
}

uint32 getSampleRate( size_t i ) {
	BOOST_ASSERT( i < getSampleRateCount() );
	return SAMPLE_RATES[i];
}
/*
 * ThreadedEventBase.h
 * -------------------
 * Description: Platform-independent base class for mechanism to periodically trigger an event.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef THREADED_EVENT_BASE_H__DEFINED__
#define THREADED_EVENT_BASE_H__DEFINED__

#include "Source/Common/Common.h"

// Platform-independent public interface
// All methods should be overridden in platform-specific implementation
// However, since runtime virtualization is not used, no methods here are marked as virtual
class ThreadedEventBase : private boost::noncopyable {
public:
	// Event function prototype
	// void Event()
	typedef fastdelegate::FastDelegate0<> Event;

	// Setters, not to be called after initialization
	void setEvent( Event event );
	void setPeriod( uint32 periodMs );

	// Getters
	Event getEvent() const;
	uint32 getPeriodMs() const;

	// Used to start/stop the thread
	bool start();
	bool stop();
	bool isRunning() const;

	// Causes event to trigger on thread
	void triggerNow();


protected:
	ThreadedEventBase();
};

#endif // THREADED_EVENT_BASE_H__DEFINED__
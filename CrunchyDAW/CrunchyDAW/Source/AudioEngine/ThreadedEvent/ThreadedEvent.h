/*
 * ThreadedEvent.h
 * -------------------
 * Description: Includes the correct implementation of periodic events based on the platform.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef THREADED_EVENT_H__DEFINED__
#define THREADED_EVENT_H__DEFINED__

#include "Source/Common/Common.h"

#if CURRENT_PLATFORM == PLATFORM_WINDOWS
#include "ThreadedEventWindows.h"
#elif CURRENT_PLATFORM == PLATFORM_APPLE
#error Not yet implemented!
#elif CURRENT_PLATFORM == PLATFORM_LINUX
#error Not yet implemented!
#else // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
#error Unsupported platform
#endif // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX

#endif // THREADED_EVENT_H__DEFINED__
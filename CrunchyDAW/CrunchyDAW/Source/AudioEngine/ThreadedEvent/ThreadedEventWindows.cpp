#include "ThreadedEventWindows.h"

#include <xutility>

#if CURRENT_PLATFORM == PLATFORM_WINDOWS

ThreadedEvent::ThreadedEvent()
	: event( NULL )
	, periodMs( 0 )
	, running( false )
	, timerEvent( NULL )
	, triggerEvent( NULL )
	, terminationEvent( NULL ) {
}

ThreadedEvent::~ThreadedEvent() {
	if (isRunning()) {
		stop();
	}

	// Make sure we've cleaned up properly so we don't leak
	BOOST_ASSERT( !timerEvent && !triggerEvent && !terminationEvent );
}

void ThreadedEvent::setEvent( Event event ) {
	BOOST_ASSERT( !isRunning() );
	this->event = event;
}

void ThreadedEvent::setPeriod( uint32 periodMs ) {
	BOOST_ASSERT( !isRunning() );
	this->periodMs = periodMs;
}

ThreadedEvent::Event ThreadedEvent::getEvent() const {
	return event;
}

uint32 ThreadedEvent::getPeriodMs() const {
	return periodMs;
}

bool ThreadedEvent::start() {
	if (isRunning()) {
		return false;
	}

	// Create events for waking up the thread
	triggerEvent = CreateEvent( NULL, FALSE, FALSE, NULL );
	terminationEvent = CreateEvent( NULL, FALSE, FALSE, NULL );

	eventThread = boost::thread( &ThreadedEvent::eventThreadMain, this );

	running = true;

	// Start a timer if a period has been specified
	if (getPeriodMs() != 0) {
		timerEvent = CreateWaitableTimer( NULL, FALSE, NULL );
		LARGE_INTEGER initialTime100Ns;
		// convert to nanoseconds; negative means relative to now
		initialTime100Ns.QuadPart = -static_cast<LONGLONG>( periodMs ) * 10000;
		SetWaitableTimer( timerEvent, &initialTime100Ns, static_cast<LONG>( periodMs ), NULL, NULL, FALSE );
	}

	return true;
}

bool ThreadedEvent::stop() {
	if (!isRunning()) {
		return false;
	}

	// Stop the timer
	if (timerEvent) {
		CancelWaitableTimer( timerEvent );
	}

	// Set the termination event
	SetEvent( terminationEvent );

	// To be safe, wait until the thread terminates
	eventThread.join();

	// Cleanup all our event objects
	if (timerEvent) {
		CloseHandle( timerEvent );
	}
	CloseHandle( triggerEvent );
	CloseHandle( terminationEvent );
	timerEvent = NULL;
	triggerEvent = NULL;
	terminationEvent = NULL;

	running = false;

	return true;
}

bool ThreadedEvent::isRunning() const {
	return running;
}

void ThreadedEvent::triggerNow() {
	BOOST_ASSERT( isRunning() );
	SetEvent( triggerEvent );
}

void ThreadedEvent::eventThreadMain() {
	bool terminate = false;
	while (!terminate) {
		// Wait for any of the possible events to trigger
		HANDLE waitHandles[3] = { terminationEvent, triggerEvent, timerEvent };
		DWORD result = WaitForMultipleObjects( timerEvent ? 3 : 2, waitHandles, FALSE, INFINITE );
		switch (result) {
		case WAIT_OBJECT_0:
			terminate = true;
			break;
		default:
			// Trigger the event
			if (event) {
				event();
			}
			break;
		}
	}
}

#endif // CURRENT_PLATFORM == PLATFORM_WINDOWS
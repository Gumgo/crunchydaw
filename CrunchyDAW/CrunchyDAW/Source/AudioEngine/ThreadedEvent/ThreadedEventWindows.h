/*
 * ThreadedEventBase.h
 * -------------------
 * Description: Windows implementation of threaded events.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef THREADED_EVENT_WINDOWS_H__DEFINED__
#define THREADED_EVENT_WINDOWS_H__DEFINED__

#include "Source/Common/Common.h"

#if CURRENT_PLATFORM == PLATFORM_WINDOWS

#include "ThreadedEventBase.h"

#include <boost/thread.hpp>

class ThreadedEvent : public ThreadedEventBase {
public:
	ThreadedEvent();
	~ThreadedEvent();

	void setEvent( Event event );
	void setPeriod( uint32 periodMs );

	Event getEvent() const;
	uint32 getPeriodMs() const;

	bool start();
	bool stop();
	bool isRunning() const;

	void triggerNow();

private:
	Event event;
	uint32 periodMs;

	boost::thread eventThread;
	void eventThreadMain();
	bool running;

	// Windows events for timing/triggering (not the same as the "threaded event")
	HANDLE timerEvent;
	HANDLE triggerEvent;
	HANDLE terminationEvent;
};

#endif // CURRENT_PLATFORM == PLATFORM_WINDOWS

#endif // THREADED_EVENT_WINDOWS_H__DEFINED__
/*
 * JobGraph.h
 * ----------
 * Description: A way to define a set of jobs with dependencies on each other.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef JOB_GRAPH_H__DEFINED__
#define JOB_GRAPH_H__DEFINED__

#include "Source/Common/Common.h"

#include <vector>

class JobGraphContext;

// A single job to be executed
class Job {
public:
	// Function belonging to the job, taking context pointer
	typedef fastdelegate::FastDelegate1<void *> Executor;

	Job( Executor executor, void *context );

	// Calls the executor
	void runExecutor() const;

private:
	Executor executor;
	void *context;
};

// Used to set up a directed acyclic graph of jobs to be executed
// This version of the graph provides a client interface for structure querying
// For execution, the graph (if valid) is converted into a JobGraphContext
class JobGraph {
public:
	JobGraph();

	// Clears entire graph
	void clear();

	// Adds a job to the graph and returns a unique ID used to identify that job
	uint32 addJob( const Job &job );

	// Adds a dependency between two jobs, meaning that predecessor must be executed before successor
	// Nothing happens if the dependency already exists
	// Asserts that both indices are valid
	void addDependency( uint32 predecessor, uint32 successor );

	// Returns number of jobs
	uint32 getJobCount() const;

	// Get counts of predecessors/successors
	uint32 getPredecessorCount( uint32 jobId ) const;
	uint32 getSuccessorCount( uint32 jobId ) const;

	// Traverse predecessor/successor lists
	// These return dependency indices, NOT the job IDs themselves, or NO_INDEX
	uint32 getFirstPredecessorDependencyIndex( uint32 jobId ) const;
	uint32 getFirstSuccessorDependencyIndex( uint32 jobId ) const;
	uint32 getNextDependencyIndex( uint32 depIndex ) const;

	// Returns the job ID associated with a dependency index
	uint32 getJobIdFromDependencyIndex( uint32 depIndex ) const;

	// Returns true if this graph is valid, meaning it has no cycles
	// Not const, because it uses some internal data structures to avoid allocation each time this is called
	bool isGraphValid();

	// Creates a context from the job graph which is ready for execution by the JobExecutor
	// Asserts that the graph is valid because isGraphValid() should always be called first
	void createContext( JobGraphContext &out );

private:
	struct JobInternal {
		JobInternal( const Job &job );

		Job job;
		uint32 predecessorCount;
		uint32 predecessorListStart;
		uint32 successorCount;
		uint32 successorListStart;

		// Remaining successor count, used during graph verification
		uint32 remainingPredecessorCount;
	};

	struct Dependency {
		uint32 jobId;	// ID of dependency job
		uint32 next;	// Next dependency in list, or INDEX_NONE
	};

	std::vector<JobInternal> jobs;
	// For correctness, we use uint32 instead of size_t for successor indices
	// This is because we will eventually store dependency counts as uint32 due to atomic operations
	std::vector<Dependency> dependencies;

	// Used to verify correctness
	std::vector<uint32> jobsWithNoPredecessors;
};

#endif // JOB_GRAPH_H__DEFINED__
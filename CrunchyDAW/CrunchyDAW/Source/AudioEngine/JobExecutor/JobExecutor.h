/*
 * JobExecutor.h
 * -------------
 * Description: Provides a system for executing job graphs, using parallelism where possible.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef JOB_EXECUTOR_H__DEFINED__
#define JOB_EXECUTOR_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/Common/Atomics.h"
#include "JobGraph.h"

#include <boost/lockfree/queue.hpp>
#include <vector>

// A job graph context is a job graph which can be executed because it stores execution state
class JobGraphContext : private boost::noncopyable {
public:
	friend class JobGraph;		// So that the JobGraphContext can be built
	friend class JobExecutor;	// So that the JobGraphContext can be executed

private:
	struct JobInternal {
		JobInternal( const Job &job );

		// Static data that does not change during execution
		Job job;
		uint32 successorListStart;
		uint32 successorListSize;
		uint32 predecessorCount;

		// Data which changes during execution
		AtomicInt32 remainingPredecessors;
	};

	AtomicInt32 remainingJobs;				// Total number of remaining jobs

	std::vector <JobInternal> jobs;			// List of jobs
	std::vector <uint32> successorLists;	// Lists of successors for each job
	std::vector <uint32> initialJobs;		// Jobs with no predecessors
};

class JobExecutor : private boost::noncopyable {
public:
	JobExecutor();
	~JobExecutor();

	// Settings for the job executor
	struct Settings {
		uint32 threadCount;	// Number of threads to create in thread pool
	};

	// Creates a thread pool and starts threads
	void start( const Settings &settings );
	// Stops threads
	void stop();
	// Returns whether threads are running
	bool isRunning() const;

	// Executes a job graph context
	// Returns a stopwatch containing the amount of time the jobs took to execute
	Stopwatch executeJobGraph( JobGraphContext &context );

private:
	// Threads in the thread pool
	struct ThreadContext {
		ThreadContext();

		// Move constructor and assignment
		ThreadContext( ThreadContext &&other );
		ThreadContext &operator=( ThreadContext &&other );

		boost::thread thread;

		// Used to signal thread
		boost::mutex mutex;
		boost::condition_variable threadSignal;

		uint32 flags;
		static const uint32 FLAG_JOBS_READY	= 0x1;
		static const uint32 FLAG_TERMINATE	= 0x2;
	};

	JobGraphContext *jobGraphContext;

	std::vector <ThreadContext> threads;
	bool running;

	// Queue of jobs to be executed; jobs are only added when all successors have been executed
	boost::lockfree::queue<uint32> jobQueue;
	// Reading this index from the queue means the thread should return to the waiting state
	static const uint32 ALL_JOBS_COMPLETE = INDEX_NONE<uint32>::value;

	boost::mutex executionFinishedMutex;
	boost::condition_variable executionFinishedSignal;
	bool executionFinished;

	// Thread main function takes thread ID in pool
	void threadMain( uint32 id );
	void executeJobQueue();
};

#endif // JOB_EXECUTOR_H__DEFINED__
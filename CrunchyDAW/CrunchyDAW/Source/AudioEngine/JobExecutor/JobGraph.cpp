#include "JobGraph.h"
#include "JobExecutor.h"

Job::Job( Executor executor, void *context )
	: executor( executor )
	, context( context ) {
	BOOST_ASSERT( executor );
}

void Job::runExecutor() const {
	this->executor( context );
}

JobGraph::JobGraph() {
}

JobGraph::JobInternal::JobInternal( const Job &job )
	: job( job )
	, predecessorCount( 0 )
	, predecessorListStart( INDEX_NONE<uint32>::value )
	, successorCount( 0 )
	, successorListStart( INDEX_NONE<uint32>::value ) {
}

void JobGraph::clear() {
	jobs.clear();
	dependencies.clear();
}

uint32 JobGraph::addJob( const Job &job ) {
	JobInternal jobInternal( job );
	uint32 index = static_cast<uint32>( jobs.size() );
	jobs.push_back( jobInternal );
	return index;
}

void JobGraph::addDependency( uint32 predecessor, uint32 successor ) {
	BOOST_ASSERT( predecessor < jobs.size() );
	BOOST_ASSERT( successor < jobs.size() );

	// Check if dependency already exists
	size_t sNode = jobs[predecessor].successorListStart;
	while (sNode != INDEX_NONE<uint32>::value) {
		const Dependency &d = dependencies[sNode];
		if (d.jobId == successor) {
			return;
		}
	}

#if ASSERTS_ENABLED == 1
	// Assert that the dependency doesn't exist in the opposite direction
	size_t pNode = jobs[successor].predecessorListStart;
	while (pNode != INDEX_NONE<uint32>::value) {
		const Dependency &d = dependencies[pNode];
		BOOST_ASSERT( d.jobId != predecessor );
	}
#endif // ASSERTS_ENABLED == 1

	// Add to front of successor list inside predecessor job
	Dependency suc;
	suc.jobId = successor;
	suc.next = jobs[predecessor].successorListStart;
	// Add to list of all dependencies
	jobs[predecessor].successorListStart = static_cast<uint32>( dependencies.size() );
	dependencies.push_back( suc );
	// Increment number of successors for the predecessor
	++jobs[predecessor].successorCount;

	// Add to front of predecessor list inside successor job
	Dependency pred;
	pred.jobId = predecessor;
	pred.next = jobs[successor].predecessorListStart;
	// Add to list of all dependencies
	jobs[successor].predecessorListStart = static_cast<uint32>( dependencies.size() );
	dependencies.push_back( pred );
	// Increment number of predecessors for the successor
	++jobs[successor].predecessorCount;
}

bool JobGraph::isGraphValid() {
	jobsWithNoPredecessors.clear();
	uint32 jobCount = static_cast<uint32>( jobs.size() );
	uint32 totalEdgeCount = 0;

	// Verify that the graph is valid by making sure there are no cycles
	// First find all jobs with no incoming edges
	for (uint32 i = 0; i < jobCount; ++i) {
		jobs[i].remainingPredecessorCount = jobs[i].predecessorCount;
		totalEdgeCount += jobs[i].predecessorCount;
		if (jobs[i].predecessorCount == 0) {
			jobsWithNoPredecessors.push_back( i );
		}
	}

	// Total edge count is half the number of dependencies, because dependencies are counted in both directions
	BOOST_ASSERT( totalEdgeCount == dependencies.size() / 2 );
	uint32 remainingEdgeCount = totalEdgeCount;

	while (!jobsWithNoPredecessors.empty()) {
		uint32 j = jobsWithNoPredecessors.back();
		jobsWithNoPredecessors.pop_back();
		// For each successor, "remove" the edge to that job
		uint32 next = jobs[j].successorListStart;
		while (next != INDEX_NONE<uint32>::value) {
			const Dependency &suc = dependencies[next];
			// "Remove" the edge, and if the incoming edge count gets to 0, add to the list
			--jobs[suc.jobId].remainingPredecessorCount;
			--remainingEdgeCount;
			if (jobs[suc.jobId].remainingPredecessorCount == 0) {
				jobsWithNoPredecessors.push_back( suc.jobId );
			}

			// Keep traversing list
			next = suc.next;
		}
	}

	return (remainingEdgeCount == 0);
}

uint32 JobGraph::getJobCount() const {
	return static_cast<uint32>( jobs.size() );
}

uint32 JobGraph::getPredecessorCount( uint32 jobId ) const {
	BOOST_ASSERT( jobId < getJobCount() );
	return jobs[jobId].predecessorCount;
}

uint32 JobGraph::getSuccessorCount( uint32 jobId ) const {
	BOOST_ASSERT( jobId < getJobCount() );
	return jobs[jobId].successorCount;
}

uint32 JobGraph::getFirstPredecessorDependencyIndex( uint32 jobId ) const {
	BOOST_ASSERT( jobId < getJobCount() );
	return jobs[jobId].predecessorListStart;
}

uint32 JobGraph::getFirstSuccessorDependencyIndex( uint32 jobId ) const {
	BOOST_ASSERT( jobId < getJobCount() );
	return jobs[jobId].successorListStart;
}

uint32 JobGraph::getNextDependencyIndex( uint32 depIndex ) const {
	BOOST_ASSERT( depIndex < dependencies.size() );
	return dependencies[depIndex].next;
}

uint32 JobGraph::getJobIdFromDependencyIndex( uint32 depIndex ) const {
	BOOST_ASSERT( depIndex < dependencies.size() );
	return dependencies[depIndex].jobId;
}

void JobGraph::createContext( JobGraphContext &out ) {
	BOOST_ASSERT( isGraphValid() );

	// First clear the output graph
	out.jobs.clear();
	out.successorLists.clear();
	out.initialJobs.clear();

	out.jobs.reserve( jobs.size() );
	// There are two dependencies per edge, one in each direction
	out.successorLists.reserve( dependencies.size() / 2 );

	// Add each job
	for (size_t i = 0; i < jobs.size(); ++i) {
		JobGraphContext::JobInternal contextJob( jobs[i].job );
		// Set up the job's contiguous list of edges
		contextJob.successorListStart = static_cast<uint32>( out.successorLists.size() );
		contextJob.successorListSize = 0;
		// Add each edge to the context
		uint32 next = jobs[i].successorListStart;
		while (next != INDEX_NONE<uint32>::value) {
			const Dependency &suc = dependencies[next];
			out.successorLists.push_back( suc.jobId );
			++contextJob.successorListSize;

			next = suc.next;
		}

		out.jobs.push_back( contextJob );
	}
}
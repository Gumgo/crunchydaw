/*
 * BufferRequirementSolver.h
 * -------------------------
 * Description: Utility class used to determine the minimum number of buffers required to execute a job graph.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef BUFFER_REQUIREMENT_SOLVER_H__DEFINED__
#define BUFFER_REQUIREMENT_SOLVER_H__DEFINED__

#include "Source/Common/Common.h"
#include "JobGraph.h"

#include <vector>

// This utility class is provided with a graph and a set of "buffers descriptions". Each buffer description provides the
// set of nodes in the graph across which the buffer must exist. This class calculates how many buffers actually need
// to be allocated for job graph execution. When two buffers can possibly exist concurrently, both must be allocated,
// but if it is guaranteed that two buffer usages will never "overlap", a single buffer can be shared between both uses.
class BufferRequirementSolver {
public:
	BufferRequirementSolver();

	// Sets up the buffer requirement solver to use the provided graph
	void initialize( const JobGraph &jobGraph );

	// Starts the description for a buffer
	void startBufferDescription();
	// Adds a node onto the node list for the buffer being currently described
	void addNodeToCurrentBufferDescription( uint32 nodeId );
	// Completes the buffer's description and verifies that the node list is valid
	// Nodes in a valid list must not form 2 or more disjoint sets of nodes, and must have a single overall "parent"
	void endBufferDescription();

	// Solves for the minimum number of buffers required
	uint32 solveMinBuffersRequired();

private:
	const JobGraph *jobGraph;

	bool bufferDescriptionStarted;
	struct BufferDescription {
		// Overall parent node of buffer
		uint32 overallParentNodeId;

		// List indices of all nodes
		size_t nodeListStart;
		size_t nodeListCount;
	};

	std::vector<BufferDescription> bufferDescriptions;
	std::vector<uint32> nodeIds;

	// Returns true if all nodes in A precede all nodes in B
	bool doesAPrecedeB( size_t bufferDescriptionIndexA, size_t bufferDescriptionIndexB ) const;
	// Returns true if the two buffers are concurrent
	bool areBuffersConcurrent( size_t bufferDescriptionIndexA, size_t bufferDescriptionIndexB ) const;

	// Fills in ancestor table
	void findAllAncestors();
	// Getter/setter for ancestor table
	void setAAncestorOfB( uint32 nodeA, uint32 nodeB );
	bool isAAncestorOfB( uint32 nodeA, uint32 nodeB ) const;
	// 2D array of bools containing whether each node is an ancestor of each other node
	std::vector<bool> ancestors;
	std::vector<uint32> ancestorStack;
};

#endif // BUFFER_REQUIREMENT_SOLVER_H__DEFINED__
#include "JobExecutor.h"

JobGraphContext::JobInternal::JobInternal( const Job &job )
	: job( job ) {
}

JobExecutor::JobExecutor() {
	running = false;
}

JobExecutor::~JobExecutor() {
	stop();
}

void JobExecutor::start( const Settings &settings ) {
	if (isRunning()) {
		return;
	}

	// Start off N threads
	threads.resize( settings.threadCount );
	for (uint32 i = 0; i < settings.threadCount; ++i) {
		ThreadContext &tc = threads.back();
		tc.flags = 0;
		tc.thread = boost::thread( boost::bind( &JobExecutor::threadMain, this, i ) );
	}

	running = true;
}

void JobExecutor::stop() {
	if (isRunning()) {
		return;
	}

	// Signal each thread to stop and then join with the thread
	for (size_t i = 0; i < threads.size(); ++i) {
		{
			boost::mutex::scoped_lock lock( threads[i].mutex );
			threads[i].flags = ThreadContext::FLAG_TERMINATE;
			threads[i].threadSignal.notify_one();
		}
		threads[i].thread.join();
	}
	threads.clear();

	running = false;
}

bool JobExecutor::isRunning() const {
	return running;
}

JobExecutor::ThreadContext::ThreadContext() {
}

JobExecutor::ThreadContext::ThreadContext( ThreadContext &&other ) {
	thread.swap( other.thread );
	// Note: mutex and conditional variable NOT moved! Only call this before they are ever used
	flags = other.flags;
}

JobExecutor::ThreadContext &JobExecutor::ThreadContext::operator=( ThreadContext &&other ) {
	thread.swap( other.thread );
	// Note: mutex and conditional variable NOT moved! Only call this before they are ever used
	return *this;
}

Stopwatch JobExecutor::executeJobGraph( JobGraphContext &context ) {
	Stopwatch stopwatch;
	stopwatch.start();


	// Initialize context for execution
	context.remainingJobs.exchange( static_cast<int32>( context.jobs.size() ) );
	for (size_t i = 0; i < context.jobs.size(); ++i) {
		// Initially all jobs have all their predecessors remaining
		context.jobs[i].remainingPredecessors.exchange( context.jobs[i].predecessorCount );
	}

	// Queue the initial jobs
	for (size_t i = 0; i < context.initialJobs.size(); ++i) {
		bool success = jobQueue.push( context.initialJobs[i] );
		BOOST_ASSERT( success ); // No reason this should fail since the queue is not fixed size
	}

	{
		// Flag execution as not yet finished and set the job graph context pointer
		boost::mutex::scoped_lock lock( executionFinishedMutex );
		executionFinished = false;
		jobGraphContext = &context;
	}

	// Signal each thread that there are jobs ready to process
	for (size_t i = 0; i < threads.size(); ++i) {
		boost::mutex::scoped_lock lock( threads[i].mutex );
		threads[i].flags = ThreadContext::FLAG_JOBS_READY;
		threads[i].threadSignal.notify_one();
	}

	// Wait for execution to finish
	while (!executionFinished) {
		boost::mutex::scoped_lock lock( executionFinishedMutex );
		executionFinishedSignal.wait( lock );
	}

	stopwatch.stop();
	return stopwatch;
}

void JobExecutor::threadMain( uint32 id ) {
	bool terminate = false;

	while (!terminate) {
		// Wait on the condition variable
		bool jobsReady = false;
		{
			boost::mutex::scoped_lock lock( threads[id].mutex );
			while (threads[id].flags == 0) {
				threads[id].threadSignal.wait( lock );
			}

			switch (threads[id].flags) {
			case ThreadContext::FLAG_JOBS_READY:
				jobsReady = true;
				break;
			case ThreadContext::FLAG_TERMINATE:
				terminate = true;
				break;
			default:
				BOOST_ASSERT_MSG( false, "Unknown flag" );
			}
		}

		if (jobsReady) {
			executeJobQueue();
		}
	}
}

void JobExecutor::executeJobQueue() {
	// Loop while there are still jobs to execute
	bool allJobsComplete = false;
	while (!allJobsComplete) {
		// Obtain the next job from the queue, spinning until there is data available
		uint32 jobId;
		while (!jobQueue.pop( jobId ));

		if (jobId == ALL_JOBS_COMPLETE) {
			// We're done
			allJobsComplete = true;
		} else {
			// Execute the job, and if a successor has opened up, immediately execute that too
			bool executeJob = true;
			while (executeJob) {
				executeJob = false;

				// Decrement the number of jobs remaining - if the previous count was 1, this is the last job
				int32 oldRemainingJobs = jobGraphContext->remainingJobs.decrement();
				BOOST_ASSERT( oldRemainingJobs > 0 );
				bool lastJob = (jobGraphContext->remainingJobs.decrement() == 1);

				// Tell the other threads to stop spinning if this is the last job
				if (lastJob) {
					// Queue up N-1 ALL_JOBS_COMPLETE signals for the other threads
					for (size_t i = 0; i < threads.size()-1; ++i) {
						jobQueue.push( ALL_JOBS_COMPLETE );
					}
					allJobsComplete = true;
				}

				// Process the job
				JobGraphContext::JobInternal &job = jobGraphContext->jobs[jobId];
				job.job.runExecutor();

				// Queue up successor jobs that have opened up
				for (uint32 j = 0; j < job.successorListSize; ++j) {
					uint32 successorId = jobGraphContext->successorLists[job.successorListStart + j];

					// Determine if this job is ready to execute
					int32 oldRemainingPredecessors =
						jobGraphContext->jobs[successorId].remainingPredecessors.decrement();
					BOOST_ASSERT( oldRemainingPredecessors > 0 );
					if (oldRemainingPredecessors == 1) {
						// The job has no more predecessors
						if (executeJob) {
							// Queue up the job for other threads
							jobQueue.push( successorId );
						} else {
							// Execute the job immediately on this thread
							executeJob = true;
							jobId = successorId;
						}
					}
				}

				// If this is the last job, notify the main thread
				if (lastJob) {
					BOOST_ASSERT( !executeJob );

					boost::mutex::scoped_lock lock( executionFinishedMutex );
					executionFinished = true;
					executionFinishedSignal.notify_one();
				}
			}
		}
	}
}
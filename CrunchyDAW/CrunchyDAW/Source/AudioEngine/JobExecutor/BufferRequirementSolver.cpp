#include "BufferRequirementSolver.h"

BufferRequirementSolver::BufferRequirementSolver()
	: jobGraph( NULL )
	, bufferDescriptionStarted( false ) {
}

void BufferRequirementSolver::initialize( const JobGraph &jobGraph ) {
	this->jobGraph = &jobGraph;
	bufferDescriptionStarted = false;
	bufferDescriptions.clear();
	nodeIds.clear();
}

void BufferRequirementSolver::startBufferDescription() {
	BOOST_ASSERT( jobGraph );
	BOOST_ASSERT( !bufferDescriptionStarted );

	BufferDescription bd;
	bd.nodeListStart = nodeIds.size();
	bd.nodeListCount = 0;
	bufferDescriptions.push_back( bd );

	bufferDescriptionStarted = true;
}

void BufferRequirementSolver::addNodeToCurrentBufferDescription( uint32 nodeId ) {
	BOOST_ASSERT( jobGraph );
	BOOST_ASSERT( bufferDescriptionStarted );
	BOOST_ASSERT( !bufferDescriptions.empty() );
	BOOST_ASSERT( nodeId < jobGraph->getJobCount() );

	++bufferDescriptions.back().nodeListCount;
	nodeIds.push_back( nodeId );
}

void BufferRequirementSolver::endBufferDescription() {
	BOOST_ASSERT( jobGraph );
	BOOST_ASSERT( bufferDescriptionStarted );
	BOOST_ASSERT( !bufferDescriptions.empty() );

	// Verify that the node list has a single overall "parent"; this also verifies that it is a single set and not 2 or
	// more disjoint sets, because 2 sets would have at least 2 parents
	BufferDescription &bd = bufferDescriptions.back();
	BOOST_ASSERT( bd.nodeListCount > 0 );

	bd.overallParentNodeId = INDEX_NONE<uint32>::value;
	size_t nodeListEnd = bd.nodeListStart + bd.nodeListCount;
	for (size_t i = bd.nodeListStart; i < nodeListEnd; ++i) {
		if (jobGraph->getPredecessorCount( nodeIds[i] ) == 0) {
			BOOST_ASSERT( bd.overallParentNodeId == INDEX_NONE<uint32>::value );
			bd.overallParentNodeId = nodeIds[i];
		}
	}
	BOOST_ASSERT( bd.overallParentNodeId != INDEX_NONE<uint32>::value );

	bufferDescriptionStarted = false;
}

uint32 BufferRequirementSolver::solveMinBuffersRequired() {
	// O(n^2)
	BOOST_ASSERT( jobGraph );
	BOOST_ASSERT( !bufferDescriptionStarted );

	// We want to find the minimum number of buffers required to execute the job. Any buffers which need to exist
	// concurrently must both be allocated. However, if there are two buffers that cannot ever exist at the same time,
	// we can share a single buffer between both usages. We determine whether two buffers are concurrent by checking
	// whether either comes entirely before the other. We check this by determining whether all nodes in buffer A are
	// ancestors (i.e. can be reached by following graph edges in "reverse") of all nodes in buffer B. (Buffers are
	// assumed to be created at a single node and branch out, so really we only need to check against the overall parent
	// node of B.) To find all pairs of potentially shared buffers, we traverse the list of buffers and compare to each
	// one which already has been traversed, and we don't increment the count if the buffer is capable of being shared
	// with any existing buffer.

	uint32 count = 0;
	// Construct a table of bools which describes ancestor relationships between all nodes
	findAllAncestors();

	// Go through the list of buffers
	for (size_t a = 0; a < bufferDescriptions.size(); ++a) {
		// Assume initially that we're concurrent with every single buffer checked so far
		bool concurrentWithAll = true;

		// Check concurrency with each buffer until we find one we're not concurrent with
		for (size_t b = 0; concurrentWithAll && b < a; ++b) {
			if (!areBuffersConcurrent( a, b )) {
				concurrentWithAll = false;
			}
		}

		// If we are concurrent with at least one buffer, it means we don't need a separate additional buffer
		// Otherwise, there is no sharing that can occur
		if (concurrentWithAll) {
			++count;
		}
	}

	return count;
}

bool BufferRequirementSolver::doesAPrecedeB( size_t bufferDescriptionIndexA, size_t bufferDescriptionIndexB ) const {
	// O(n)
	BOOST_ASSERT( bufferDescriptionIndexA < bufferDescriptions.size() );
	BOOST_ASSERT( bufferDescriptionIndexB < bufferDescriptions.size() );

	const BufferDescription &bdA = bufferDescriptions[bufferDescriptionIndexA];
	const BufferDescription &bdB = bufferDescriptions[bufferDescriptionIndexB];
	size_t start = bdA.nodeListStart;
	size_t end = start + bdA.nodeListCount;
	for (size_t i = start; i < end; ++i) {
		if (!isAAncestorOfB( nodeIds[i], bdB.overallParentNodeId )) {
			return false;
		}
	}

	return true;
}

bool BufferRequirementSolver::areBuffersConcurrent(
	// O(n)
	size_t bufferDescriptionIndexA, size_t bufferDescriptionIndexB ) const {
	// Not concurrent if one comes before the other
	return !doesAPrecedeB( bufferDescriptionIndexA, bufferDescriptionIndexB ) &&
		!doesAPrecedeB( bufferDescriptionIndexB, bufferDescriptionIndexA );
}

void BufferRequirementSolver::findAllAncestors() {
	// O(n^2) time, O(n^2) space
	ancestors.clear();
	ancestors.resize( jobGraph->getJobCount() * jobGraph->getJobCount(), false );

	for (uint32 nodeId = 0; nodeId < jobGraph->getJobCount(); ++nodeId) {
		ancestorStack.clear();

		// Set starting node as an ancestor of itself
		setAAncestorOfB( nodeId, nodeId );
		ancestorStack.push_back( nodeId );

		// Recursively visit predecessor nodes
		while (!ancestorStack.empty()) {
			uint32 ancestorNodeId = ancestorStack.back();
			ancestorStack.pop_back();

			// Push all predecessors recursively
			for (uint32 predIndex = jobGraph->getFirstPredecessorDependencyIndex( ancestorNodeId );
				 predIndex != INDEX_NONE<uint32>::value;
				 predIndex = jobGraph->getNextDependencyIndex( predIndex )) {
				// Obtain the node ID from the predecessor
				uint32 predNodeId = jobGraph->getJobIdFromDependencyIndex( predIndex );

				// Add this node to the stack only if we haven't already added it before
				if (!isAAncestorOfB( predNodeId, nodeId )) {
					setAAncestorOfB( predNodeId, nodeId );
					ancestorStack.push_back( predNodeId );
				}
			}
		}
	}
}

void BufferRequirementSolver::setAAncestorOfB( uint32 nodeA, uint32 nodeB ) {
	BOOST_ASSERT( nodeA < jobGraph->getJobCount() );
	BOOST_ASSERT( nodeB < jobGraph->getJobCount() );
	ancestors[nodeB * jobGraph->getJobCount() + nodeA] = true;
}

bool BufferRequirementSolver::isAAncestorOfB( uint32 nodeA, uint32 nodeB ) const {
	BOOST_ASSERT( nodeA < jobGraph->getJobCount() );
	BOOST_ASSERT( nodeB < jobGraph->getJobCount() );
	return ancestors[nodeB * jobGraph->getJobCount() + nodeA];
}
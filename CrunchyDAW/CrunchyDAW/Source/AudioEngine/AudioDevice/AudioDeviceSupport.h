/*
 * AudioDeviceSupport.h
 * --------------------
 * Description: Determines which audio devices are available on which platforms.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef AUDIO_DEVICE_SUPPORT_H__DEFINED__
#define AUDIO_DEVICE_SUPPORT_H__DEFINED__

#include "Source/Common/Common.h"

#if CURRENT_PLATFORM == PLATFORM_WINDOWS
#define DEVICE_DIRECT_SOUND_SUPPORTED	1
#define DEVICE_ASIO_SUPPORTED			1
#elif CURRENT_PLATFORM == PLATFORM_APPLE
#error Add devices here!
#elif CURRENT_PLATFORM == PLATFORM_LINUX
#error Add devices here!
#else // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX
#endif // !PLATFORM_WINDOWS && !PLATFORM_APPLE && !PLATFORM_LINUX

#endif // AUDIO_DEVICE_SUPPORT_H__DEFINED__
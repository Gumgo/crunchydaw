#include "AudioDeviceDirectSound.h"

#if DEVICE_DIRECT_SOUND_SUPPORTED == 1

#include <MMReg.h>

bool setupWaveFormat( const AudioDeviceSettings &settings, WAVEFORMATEXTENSIBLE &outWaveFormat ) {
	ZERO_STRUCT( &outWaveFormat );

	// Set up standard properties
	const SampleFormatProperties &sfp = getSampleFormatProperties( settings.sampleFormat );
	outWaveFormat.Format.wFormatTag = sfp.isFloat ? WAVE_FORMAT_IEEE_FLOAT : WAVE_FORMAT_PCM;
	outWaveFormat.Format.nChannels = settings.channels;
	outWaveFormat.Format.nSamplesPerSec = settings.sampleRate;
	outWaveFormat.Format.nAvgBytesPerSec = settings.getBytesPerSecond();
	outWaveFormat.Format.nBlockAlign = static_cast<WORD>( settings.getBytesPerFrame() );
	outWaveFormat.Format.wBitsPerSample = static_cast<WORD>( sfp.bitsPerSample );
	outWaveFormat.Format.cbSize = 0;

	// In some cases we need to use extended wave structure
	if ((sfp.bitsPerSample > 16 && sfp.isInt) || settings.channels > 2) {
		outWaveFormat.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
		outWaveFormat.Format.cbSize = sizeof( WAVEFORMATEXTENSIBLE ) - sizeof( WAVEFORMATEX );
		outWaveFormat.Samples.wValidBitsPerSample = outWaveFormat.Format.wBitsPerSample;
		switch (settings.channels) {
		case 1:
			outWaveFormat.dwChannelMask =
				SPEAKER_FRONT_CENTER;
			break;
		case 2:
			outWaveFormat.dwChannelMask =
				SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT;
			break;
		case 3:
			outWaveFormat.dwChannelMask =
				SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT | SPEAKER_BACK_CENTER;
			break;
		case 4:
			outWaveFormat.dwChannelMask =
				SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT | SPEAKER_BACK_LEFT | SPEAKER_BACK_RIGHT;
			break;
		default:
			outWaveFormat.dwChannelMask = 0;
			return false;
		}

		outWaveFormat.SubFormat = sfp.isInt ? KSDATAFORMAT_SUBTYPE_PCM : KSDATAFORMAT_SUBTYPE_IEEE_FLOAT;
	}

	return true;
}

struct TypeSpecificData {
	GUID guid;
	bool emptyGuid;
};

BOOST_STATIC_ASSERT( sizeof( TypeSpecificData ) <= sizeof( AudioDeviceInfo().typeSpecificData ) );

AudioDeviceDirectSound::AudioDeviceDirectSound( const AudioDeviceInfo &deviceInfo, ThreadedEvent &audioThread )
	: AudioDevice( deviceInfo, audioThread ) {
	directSoundObject = NULL;
	primarySoundBuffer = NULL;
	mixSoundBuffer = NULL;
	bufferSizeBytes = 0;
}

std::vector<AudioDeviceInfo> AudioDeviceDirectSound::enumerateDevices() {
	std::vector<AudioDeviceInfo> devices;
	DirectSoundEnumerate( &enumerateDevicesCallback, &devices );
	return devices;
}

BOOL CALLBACK AudioDeviceDirectSound::enumerateDevicesCallback(
	LPGUID guid, LPCWSTR description, LPCWSTR module, LPVOID context ) {
	BOOST_ASSERT( context );

	if (!description) {
		return TRUE;
	}

	std::vector<AudioDeviceInfo> &devices = *(static_cast<std::vector<AudioDeviceInfo>*>( context ));
	AudioDeviceInfo info;
	info.type = AUDIO_DEVICE_TYPE_DIRECT_SOUND;
	info.name = description;
	// store GUID inside type-specific data
	TypeSpecificData *typeSpecificData = reinterpret_cast<TypeSpecificData*>( info.typeSpecificData );
	if (guid) {
		typeSpecificData->guid = *guid;
		typeSpecificData->emptyGuid = false;
	} else {
		typeSpecificData->guid = GUID();
		typeSpecificData->emptyGuid = true;
	}
	devices.push_back( info );

	return TRUE;
}

bool AudioDeviceDirectSound::openInternal( const AudioDeviceSettings &settings ) {
	bool result;

	WAVEFORMATEXTENSIBLE waveFormat;
	result = setupWaveFormat( settings, waveFormat );

	if (result) {
		// obtain the device ID from the type-specific data field
		const TypeSpecificData *typeSpecificData =
			reinterpret_cast<const TypeSpecificData*>( getDeviceInfo().typeSpecificData );
		if (DirectSoundCreate(
				typeSpecificData->emptyGuid ? NULL : &typeSpecificData->guid, &directSoundObject, NULL ) == DS_OK) {
			BOOST_ASSERT( directSoundObject );
		} else {
			result = false;
		}
	}

	if (result) {
		// set up our priority - set to highest if we're in exclusive mode
		DWORD coopLevel = settings.exclusiveMode ? DSSCL_WRITEPRIMARY : DSSCL_PRIORITY;
		if (directSoundObject->SetCooperativeLevel( (HWND)settings.mainWindowId, coopLevel ) != DS_OK) {
			result = false;
		}
	}

	if (result) {
		// Set up primary and mix buffers
		if (!settings.exclusiveMode) {
			bufferSizeBytes = (settings.getBytesPerSecond() * settings.latencyMs) / 1000;
			// Round to multiple of frame size
			bufferSizeBytes = ((bufferSizeBytes + settings.getBytesPerFrame() - 1) / settings.getBytesPerFrame()) *
				settings.getBytesPerFrame();
			// Make sure sound buffer isn't too small or big
			uint32 minBufferSize = ((DSBSIZE_MIN + settings.getBytesPerFrame() - 1) / settings.getBytesPerFrame()) *
				settings.getBytesPerFrame();
			uint32 maxBufferSize = (DSBSIZE_MAX / settings.getBytesPerFrame()) * settings.getBytesPerFrame();
			bufferSizeBytes = std::min( maxBufferSize, std::max( minBufferSize, bufferSizeBytes ) );

			DSBUFFERDESC primaryBufferDesc;
			ZERO_STRUCT( &primaryBufferDesc );
			primaryBufferDesc.dwSize = sizeof( primaryBufferDesc );
			primaryBufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
			primaryBufferDesc.dwBufferBytes = 0;
			primaryBufferDesc.dwReserved = 0;
			primaryBufferDesc.lpwfxFormat = NULL;

			DSBUFFERDESC secondaryBufferDesc;
			ZERO_STRUCT( &secondaryBufferDesc );
			secondaryBufferDesc.dwSize = sizeof( secondaryBufferDesc );
			secondaryBufferDesc.dwFlags = DSBCAPS_GLOBALFOCUS | DSBCAPS_GETCURRENTPOSITION2;
			secondaryBufferDesc.dwBufferBytes = bufferSizeBytes;
			secondaryBufferDesc.dwReserved = 0;
			secondaryBufferDesc.lpwfxFormat = &waveFormat.Format;

			if (directSoundObject->CreateSoundBuffer( &primaryBufferDesc, &primarySoundBuffer, NULL ) != DS_OK ||
				directSoundObject->CreateSoundBuffer( &secondaryBufferDesc, &mixSoundBuffer, NULL ) != DS_OK) {
				result = false;
			}

			if (result) {
				result = (primarySoundBuffer->SetFormat( &waveFormat.Format ) == DS_OK);
			}
		} else {
			// Exclusive mode writes directly to primary buffer
			DSBUFFERDESC primaryBufferDesc;
			ZERO_STRUCT( &primaryBufferDesc );
			primaryBufferDesc.dwSize = sizeof( primaryBufferDesc );
			primaryBufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
			primaryBufferDesc.dwBufferBytes = 0;
			primaryBufferDesc.dwReserved = 0;
			primaryBufferDesc.lpwfxFormat = NULL;

			if (directSoundObject->CreateSoundBuffer( &primaryBufferDesc, &primarySoundBuffer, NULL ) != DS_OK) {
				result = false;
			}

			if (result) {
				result = (primarySoundBuffer->SetFormat( &waveFormat.Format ) == DS_OK);
			}

			if (result) {
				DSBCAPS caps;
				caps.dwSize = sizeof( caps );
				if (primarySoundBuffer->GetCaps( &caps ) != DS_OK) {
					result = false;
				} else {
					// Avoid logic to pick which buffer to use by just assigning both to the same object
					bufferSizeBytes = caps.dwBufferBytes;
					primarySoundBuffer->AddRef();
					mixSoundBuffer = primarySoundBuffer;

					// Make sure the buffer size is a multiple of the frame size
					result = ((bufferSizeBytes % settings.getBytesPerFrame()) == 0);
				}
			}
		}
	}

	if (result) {
		// Zero the buffer
		const SampleFormatProperties &sfp = getSampleFormatProperties( settings.sampleFormat );
		LPVOID ptr[2];
		DWORD size[2];
		HRESULT hr = mixSoundBuffer->Lock( 0, bufferSizeBytes, &ptr[0], &size[0], &ptr[1], &size[1], 0 );
		if (hr == DSERR_BUFFERLOST) {
			// Try to restore the buffer
			mixSoundBuffer->Restore();
			hr = mixSoundBuffer->Lock( 0, bufferSizeBytes, &ptr[0], &size[0], &ptr[1], &size[1], 0 );
		}

		if (hr == DS_OK) {
			for (size_t i = 0; i < 2; ++i) {
				if (ptr[i] && size[i] > 0) {
					memset( ptr[i], sfp.silenceByte, size[i] );
				}
			}
			hr = mixSoundBuffer->Unlock( ptr[0], size[0], ptr[1], size[1] );
			if (hr != DS_OK) {
				result = false;
			}
		}

		// Initially we have not written to the buffer
		bufferWritePosition = NO_WRITE_POSITION;
		localBuffer.resize( bufferSizeBytes );
	}

	if (result) {
		// Make sure our update interval isn't too large; limit to half the buffer size
		uint32 updateIntervalMs = std::min( settings.updateIntervalMs,
			(500 * bufferSizeBytes) / settings.getBytesPerSecond() );
		updateIntervalMs = std::max( 1u, updateIntervalMs );

		// Start the update event
		ThreadedEvent &audioThread = getAudioThread();
		BOOST_ASSERT( !audioThread.isRunning() );
		audioThread.setPeriod( updateIntervalMs );
		audioThread.setEvent( ThreadedEvent::Event( this, &AudioDeviceDirectSound::fillBuffer ) );
		result = audioThread.start();
	}

	// Cleanup if failed
	if (!result) {
		close();
	}

	return result;
}

bool AudioDeviceDirectSound::closeInternal() {
	getAudioThread().stop();

	if (primarySoundBuffer) {
		primarySoundBuffer->Release();
		primarySoundBuffer = NULL;
	}

	if (mixSoundBuffer) {
		mixSoundBuffer->Release();
		mixSoundBuffer = NULL;
	}

	if (directSoundObject) {
		directSoundObject->Release();
		directSoundObject = NULL;
	}

	bufferSizeBytes = 0;

	return true;
}

bool AudioDeviceDirectSound::isOpenInternal() const {
	return (directSoundObject != NULL);
}

void AudioDeviceDirectSound::fillBuffer() {
	BOOST_ASSERT( mixSoundBuffer );

	bool result = true;
	uint32 bytesPerFrame = getDeviceSettings().getBytesPerFrame();

	// Obtain the play and write pointers
	DWORD playPointer;
	DWORD writePointer;
	HRESULT hr = mixSoundBuffer->GetCurrentPosition( &playPointer, &writePointer );
	if (hr != DS_OK) {
		if (getAudioEventHandler()) {
			getAudioEventHandler()( AUDIO_EVENT_DEVICE_ERROR );
		}
		result = false;
	}

	if (result) {
		// If this is the first write, initialize the write position at the write pointer
		if (bufferWritePosition == NO_WRITE_POSITION) {
			bufferWritePosition = uint32( writePointer );
			// Make sure we're aligned with frame size
			bufferWritePosition = ((bufferWritePosition + bytesPerFrame + 1) / bytesPerFrame) * bytesPerFrame;
			if (bufferWritePosition == bufferSizeBytes) {
				bufferWritePosition = 0;
			}
			BOOST_ASSERT( bufferWritePosition < bufferSizeBytes );
		}

		// Determine how much data to write - from write position to the current play pointer
		uint32 writeBytes;
		if (playPointer < bufferWritePosition) {
			writeBytes = playPointer + (bufferSizeBytes - bufferWritePosition);
		} else {
			writeBytes = playPointer - bufferWritePosition;
		}

		// Align write amount with frame size
		uint32 writeFrames = writeBytes / bytesPerFrame;
		writeBytes = writeFrames * bytesPerFrame;

		// Fill buffer with audio data
		if (writeFrames > 0 && getAudioProvider()) {
			getAudioProvider()( getDeviceSettings(), writeFrames, &localBuffer.front() );
		}

		// Lock buffer and write
		LPVOID ptr[2];
		DWORD size[2];
		HRESULT hr = mixSoundBuffer->Lock( bufferWritePosition, writeBytes, &ptr[0], &size[0], &ptr[1], &size[1], 0 );
		if (hr == DSERR_BUFFERLOST) {
			// Try to restore the buffer
			mixSoundBuffer->Restore();
			hr = mixSoundBuffer->Lock( bufferWritePosition, writeBytes, &ptr[0], &size[0], &ptr[1], &size[1], 0 );
			if (hr != DS_OK) {
				// $TODO This could be a buffer underrun maybe? Or other error
			}
		}

		if (hr == DS_OK) {
			BOOST_ASSERT( size[0] + size[1] == writeBytes );
			// Write data to each pointer if it is not NULL
			if (ptr[0] && size[0] > 0) {
				memcpy( ptr[0], &localBuffer.front(), size[0] );
			}
			if (ptr[1] && size[1] > 0) {
				memcpy( ptr[1], &localBuffer.front() + size[0], size[1] );
			}

			if (mixSoundBuffer->Unlock( ptr[0], size[0], ptr[1], size[1] ) != DS_OK) {
				// It would be bad if we couldn't unlock the buffer
				if (getAudioEventHandler()) {
					getAudioEventHandler()( AUDIO_EVENT_DEVICE_ERROR );
				}
			}
		}

		// Increment our write position
		bufferWritePosition = (bufferWritePosition + writeBytes) % bufferSizeBytes;

		// Make sure buffer is playing, or start it first time
		bool needsPlay = false;
		DWORD status;
		mixSoundBuffer->GetStatus( &status );
		if (status & DSBSTATUS_BUFFERLOST) {
			// Restore the buffer
			mixSoundBuffer->Restore();
			needsPlay = true;
		} else if (!(status & DSBSTATUS_PLAYING)) {
			needsPlay = true;
		}

		if (needsPlay) {
			if (mixSoundBuffer->Play( 0, 0, DSBPLAY_LOOPING ) != DS_OK) {
				if (getAudioEventHandler()) {
					getAudioEventHandler()( AUDIO_EVENT_DEVICE_ERROR );
				}
			}
		}
	}

	// $TODO Determine if we underrun or took too long, and update alerts of this
	// How do we do this? Explicit timing code?
	// See Portaudio DSound code for buffer underflow detection
}

// $NOTE need to clip floats sometimes, see NeedsClippedFloat in OpenMPT

#endif // DEVICE_DIRECT_SOUND_SUPPORTED == 1
/*
 * AudioDeviceManager.h
 * --------------------
 * Description: Manages the list of available audio devices.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef AUDIO_DEVICE_MANAGER_H__DEFINED__
#define AUDIO_DEVICE_MANAGER_H__DEFINED__

#include "Source/Common/Common.h"
#include "AudioDevice.h"

#include <vector>

class AudioDeviceManager {
public:
	AudioDeviceManager();
	~AudioDeviceManager();

	void findAvailableDevices();
	const std::vector<AudioDeviceInfo> &getAvailableDevices() const;

	static AudioDevice *createAudioDevice( const AudioDeviceInfo &deviceInfo, ThreadedEvent &audioThread );

private:
	std::vector<AudioDeviceInfo> availableDevices;
};

#endif // AUDIO_DEVICE_MANAGER_H__DEFINED__
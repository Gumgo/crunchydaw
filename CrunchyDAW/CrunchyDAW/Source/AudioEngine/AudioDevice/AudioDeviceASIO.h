/*
 * AudioDeviceASIO.h
 * -------------
 * Description: ASIO class of audio drivers.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef AUDIO_DEVICE_ASIO_H__DEFINED__
#define AUDIO_DEVICE_ASIO_H__DEFINED__

#include "Source/Common/Common.h"
#include "AudioDeviceSupport.h"

#ifdef AUDIO_DEVICE_ASIO_SUPPORTED

#include "AudioDevice.h"

// $TODO

#endif // AUDIO_DEVICE_ASIO_SUPPORTED

#endif // AUDIO_DEVICE_ASIO_H__DEFINED__
#include "AudioDeviceManager.h"

#include "AudioDeviceDirectSound.h"
#include "AudioDeviceASIO.h"

AudioDeviceManager::AudioDeviceManager() {
}

AudioDeviceManager::~AudioDeviceManager() {
}

void AudioDeviceManager::findAvailableDevices() {
	availableDevices.clear();

	// Obtain the list of audio devices

#if DEVICE_DIRECT_SOUND_SUPPORTED == 1
	{
		std::vector<AudioDeviceInfo> directSoundAudioDevices = AudioDeviceDirectSound::enumerateDevices();
		for (size_t i = 0; i < directSoundAudioDevices.size(); ++i) {
			availableDevices.push_back( directSoundAudioDevices[i] );
		}
	}
#endif // DEVICE_DIRECT_SOUND_SUPPORTED == 1

#if DEVICE_ASIO_SUPPORTED == 1
	{
		// $TODO
	}
#endif // DEVICE_ASIO_SUPPORTED == 1
}

const std::vector<AudioDeviceInfo> &AudioDeviceManager::getAvailableDevices() const {
	return availableDevices;
}

AudioDevice *AudioDeviceManager::createAudioDevice( const AudioDeviceInfo &deviceInfo, ThreadedEvent &audioThread ) {
	// Create audio device based on type
	AudioDevice *result = NULL;
	switch (deviceInfo.type) {

	case AUDIO_DEVICE_TYPE_DIRECT_SOUND:
#if DEVICE_DIRECT_SOUND_SUPPORTED == 1
		result = new AudioDeviceDirectSound( deviceInfo, audioThread );
		break;
#endif // DEVICE_DIRECT_SOUND_SUPPORTED == 1

	case AUDIO_DEVICE_TYPE_ASIO:
		// $TODO: ASIO
		break;

	default:
		BOOST_ASSERT_MSG( false, "Unknown device type" );
	}

	return result;
}
/*
 * AudioDeviceDirectSound.h
 * ------------------------
 * Description: DirectSound class of audio drivers.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef AUDIO_DEVICE_DIRECT_SOUND_H__DEFINED__
#define AUDIO_DEVICE_DIRECT_SOUND_H__DEFINED__

#include "Source/Common/Common.h"
#include "AudioDeviceSupport.h"

#if DEVICE_DIRECT_SOUND_SUPPORTED == 1

#include "AudioDevice.h"

#include <dsound.h>
#include <vector>

class AudioDeviceDirectSound : public AudioDevice {
public:
	AudioDeviceDirectSound( const AudioDeviceInfo &deviceInfo, ThreadedEvent &audioThread );

	static std::vector<AudioDeviceInfo> enumerateDevices();

protected:
	bool openInternal( const AudioDeviceSettings &settings );
	bool closeInternal();
	bool isOpenInternal() const;

private:
	static BOOL CALLBACK enumerateDevicesCallback( LPGUID guid, LPCWSTR description, LPCWSTR module, LPVOID context );

	IDirectSound *directSoundObject;
	IDirectSoundBuffer *primarySoundBuffer;
	IDirectSoundBuffer *mixSoundBuffer;

	uint32 bufferSizeBytes;			// Total size of the buffer
	uint32 bufferWritePosition;		// Last position we wrote to
	static const uint32 NO_WRITE_POSITION = boost::integer_traits<uint32>::const_max;

	std::vector<uint8> localBuffer;	// Buffer to accumulate audio data in before sending to sound card

	void fillBuffer();
};

#endif // DEVICE_DIRECT_SOUND_SUPPORTED == 1

#endif // AUDIO_DEVICE_DIRECT_SOUND_H__DEFINED__
#include "AudioDevice.h"

#pragma comment( lib, "dsound.lib" )

uint32 AudioDeviceSettings::getBytesPerFrame() const {
	return (getSampleFormatProperties( sampleFormat ).bitsPerSample / 8) * channels;
}

uint32 AudioDeviceSettings::getBytesPerSecond() const {
	return getBytesPerFrame() * sampleRate;
}

const wchar_t *getApiNameFromDeviceType( AudioDeviceType type ) {
	switch (type) {
	case AUDIO_DEVICE_TYPE_DIRECT_SOUND:
		return L"DirectSound";
	case AUDIO_DEVICE_TYPE_ASIO:
		return L"ASIO";
	default:
		return L"<Unknown>";
	}
}

AudioDevice::AudioDevice(
	const AudioDeviceInfo &deviceInfo,
	ThreadedEvent &audioThread )
	: deviceInfo( deviceInfo )
	, audioThread( audioThread )
	, audioEventHandler( NULL )
	, audioProvider( NULL ) {
}

AudioDevice::~AudioDevice() {
}

bool AudioDevice::open( const AudioDeviceSettings &settings ) {
	if (isOpen()) {
		close();
	}

	if (openInternal( settings )) {
		deviceSettings = settings;
		return true;
	} else {
		return false;
	}
}

bool AudioDevice::close() {
	if (isOpen()) {
		return closeInternal();
	} else {
		return false;
	}
}

bool AudioDevice::isOpen() const {
	return isOpenInternal();
}

const AudioDeviceInfo &AudioDevice::getDeviceInfo() const {
	return deviceInfo;
}

const AudioDeviceSettings &AudioDevice::getDeviceSettings() const {
	return deviceSettings;
}

ThreadedEvent &AudioDevice::getAudioThread() {
	return audioThread;
}

const ThreadedEvent &AudioDevice::getAudioThread() const {
	return audioThread;
}

void AudioDevice::setAudioEventHandler( AudioEventHandler audioEventHandler ) {
	BOOST_ASSERT( !isOpen() );
	this->audioEventHandler = audioEventHandler;
}

AudioDevice::AudioEventHandler AudioDevice::getAudioEventHandler() const {
	return audioEventHandler;
}

void AudioDevice::setAudioProvider( AudioProvider audioProvider ) {
	BOOST_ASSERT( !isOpen() );
	this->audioProvider = audioProvider;
}

AudioDevice::AudioProvider AudioDevice::getAudioProvider() const {
	return audioProvider;
}
/*
 * SampleFormat.h
 * --------------
 * Description: Enumerates available sample formats.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef SAMPLE_FORMAT_H__DEFINED__
#define SAMPLE_FORMAT_H__DEFINED__

#include "Source/Common/Common.h"

enum SampleFormat {
	SAMPLE_FORMAT_UINT8 = 0,
	SAMPLE_FORMAT_INT16,
	SAMPLE_FORMAT_INT24,
	SAMPLE_FORMAT_INT32,
	SAMPLE_FORMAT_FLOAT32,

	SAMPLE_FORMAT_COUNT
};

struct SampleFormatProperties {
	bool isUnsigned;		// Whether this format is unsigned
	bool isInt;				// Whether this format is integer
	bool isFloat;			// Whether this format is float
	uint32 bitsPerSample;	// Number of bits per sample
	uint8 silenceByte;		// Byte value used to fill buffer with silence
};

const SampleFormatProperties & getSampleFormatProperties( SampleFormat sampleFormat );

size_t getSampleRateCount();
uint32 getSampleRate( size_t i );

#endif // SAMPLE_FORMAT_H__DEFINED__
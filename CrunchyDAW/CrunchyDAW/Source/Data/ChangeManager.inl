template <typename C>
ChangeManager::Action<C>::Action(
	typename Command<C>::Function undoCommand,
	typename Command<C>::Function redoCommand,
	const C &context )
	: context( context )
	, undoCommand( undoCommand )
	, redoCommand( redoCommand ) {
}

template <typename C>
void ChangeManager::Action<C>::callDestructor( ActionBase *action ) {
	static_cast<Action<C>*>( action )->~C();
}

template <typename C>
void ChangeManager::Action<C>::callUndo( ActionBase *action ) {
	Action<C> *typedAction = static_cast<Action<C>*>( action );
	typedAction->undoCommand( COMMAND_TYPE_UNDO, typedAction->context );
}

template <typename C>
void ChangeManager::Action<C>::callRedo( ActionBase *action ) {
	Action<C> *typedAction = static_cast<Action<C>*>( action );
	typedAction->redoCommand( COMMAND_TYPE_REDO, typedAction->context );
}

template <typename C>
void ChangeManager::pushActionAndDo(
	typename Command<C>::Function doCommand,
	typename Command<C>::Function undoCommand,
	typename Command<C>::Function redoCommand,
	const C &context ) {
	BOOST_ASSERT( !performingUndoOrRedo );
	BOOST_ASSERT( nestCount > 0 );
	BOOST_ASSERT( !changeStack.empty() );

	// Setup static ActionData - one of these exists per template type
	static const ActionData actionData = {
		sizeof( Action<C> ),		// Size
		&Action<C>::callDestructor,	// Destruction wrapper
		&Action<C>::callUndo,		// Undo wrapper
		&Action<C>::callRedo		// Redo wrapper
	};

	// Allocate space on the actionStack
	size_t alignedSize = align( actionData.size, ChunkedStack::ALIGNMENT_BITS );
	void *actionPtr = actionStack.pushTop( alignedSize );

	// Construct using placement new
	Action<C> *action = new (actionPtr) Action<C>( undoCommand, redoCommand, context );
	action->actionData = &actionData;

	// Set up action in the list
	addToActionList( action );

	// Last, execute DO
	if (doCommand) {
		doCommand( COMMAND_TYPE_DO, context );
	}
}
#include "ChunkedStack.h"

ChunkedStack::ChunkedStack( size_t chunkSize )
	: chunkSize( chunkSize ) {
	bottomChunk = NULL;
	topChunk = NULL;
}

ChunkedStack::~ChunkedStack() {
	// Free all chunks
	uint8 *chunk = bottomChunk;
	while (chunk) {
		Header *header = getHeader( chunk );
		// Make sure the end of the list is actually the last chunk
		BOOST_ASSERT( header->nextChunk || chunk == topChunk );

		uint8 *nextChunk = header->nextChunk;
		delete[] chunk;
	}
}

ChunkedStack::Header *ChunkedStack::getHeader( uint8 *chunk ) {
	BOOST_ASSERT( chunk );
	return reinterpret_cast<Header*>( chunk );
}

void ChunkedStack::allocateChunkForSize( size_t size, bool top ) {
	BOOST_ASSERT( isAligned( size, ALIGNMENT_BITS ) );
	// Make sure there is space for both the header and the data
	size_t spaceRequired = FIRST_VALID_OFFSET + size;
	size_t allocSize = std::min( spaceRequired, chunkSize );

	uint8 *chunk = new uint8[allocSize];

	// Make sure chunk is aligned
	uintptr_t ptr = reinterpret_cast<uintptr_t>( chunk );
	// Static cast is okay even if it loses data, since we only care about bottom bits
	BOOST_ASSERT( isAligned( static_cast<size_t>( ptr ), ALIGNMENT_BITS ) );

	Header *header = getHeader( chunk );
	header->size = spaceRequired;
	if (top) {
		header->firstUsedOffset = header->lastUsedOffset = FIRST_VALID_OFFSET;
	} else {
		header->firstUsedOffset = header->lastUsedOffset = header->size;
	}

	// Link to other chunks
	if (!bottomChunk) {
		// This is the only chunk
		BOOST_ASSERT( !topChunk );
		bottomChunk = topChunk = chunk;
	} else {
		// Make sure this really is the top or bottom
		if (top) {
			BOOST_ASSERT( !getHeader( topChunk )->nextChunk );
			getHeader( topChunk )->nextChunk = chunk;
			topChunk = chunk;
		} else {
			BOOST_ASSERT( !getHeader( bottomChunk )->prevChunk );
			getHeader( bottomChunk )->prevChunk = chunk;
			bottomChunk = chunk;
		}
	}
}

void *ChunkedStack::pushBottom( size_t size ) {
	BOOST_ASSERT( isAligned( size, ALIGNMENT_BITS ) );

	// Check if we have enough space in the current bottom chunk
	bool enoughSpace = (bottomChunk != NULL);
	if (enoughSpace) {
		Header *header = getHeader( bottomChunk );
		enoughSpace = (FIRST_VALID_OFFSET + size <= header->firstUsedOffset);
	}

	if (!enoughSpace) {
		allocateChunkForSize( size, false );
	}

	// Put onto the bottom chunk
	Header *header = getHeader( bottomChunk );
	BOOST_ASSERT( FIRST_VALID_OFFSET + size <= header->firstUsedOffset );
	header->firstUsedOffset -= size;

#if ASSERTS_ENABLED == 1
	callSizes.push_back( size );
#endif // ASSERTS_ENABLED == 1

	return bottomChunk + header->firstUsedOffset;
}

void ChunkedStack::popBottom( size_t size ) {
	BOOST_ASSERT( isAligned( size, ALIGNMENT_BITS ) );
	BOOST_ASSERT( bottomChunk );

#if ASSERTS_ENABLED == 1
	BOOST_ASSERT( !callSizes.empty() && callSizes.back() == size );
	callSizes.pop_back();
#endif // ASSERTS_ENABLED == 1

	uint8 *chunk = bottomChunk;
	Header *header = getHeader( bottomChunk );
	// If bottom is empty, move to the next
	// We leave 1 empty chunk as padding
	if (header->firstUsedOffset == header->lastUsedOffset) {
		// Empty bottom chunks should not have space on top
		BOOST_ASSERT( header->lastUsedOffset == header->size );
		chunk = header->nextChunk;
		header = getHeader( chunk );
	}

	// Add space back
	BOOST_ASSERT( header->firstUsedOffset + size <= header->lastUsedOffset );
	header->firstUsedOffset += size;
	if (header->firstUsedOffset == header->lastUsedOffset) {
		// Chunk is empty - push both first and last to the top
		header->firstUsedOffset = header->lastUsedOffset = header->size;

		// If this isn't the bottom chunk, it means we have two empty chunks, so deallocate the current bottom
		header->prevChunk = NULL;
		delete[] bottomChunk;
		bottomChunk = chunk;
	}
}

void *ChunkedStack::pushTop( size_t size ) {
	BOOST_ASSERT( isAligned( size, ALIGNMENT_BITS ) );

	// Check if we have enough space in the current top chunk
	bool enoughSpace = (topChunk != NULL);
	if (enoughSpace) {
		Header *header = getHeader( topChunk );
		enoughSpace = (header->lastUsedOffset + size <= header->size);
	}

	if (!enoughSpace) {
		allocateChunkForSize( size, true );
	}

	// Put onto the top chunk
	Header *header = getHeader( topChunk );
	BOOST_ASSERT( header->lastUsedOffset + size <= header->size );
	void *ret = topChunk + header->lastUsedOffset;
	header->lastUsedOffset += size;

#if ASSERTS_ENABLED == 1
	callSizes.push_front( size );
#endif // ASSERTS_ENABLED == 1

	return ret;
}

void ChunkedStack::popTop( size_t size ) {
	BOOST_ASSERT( isAligned( size, ALIGNMENT_BITS ) );
	BOOST_ASSERT( bottomChunk );

#if ASSERTS_ENABLED == 1
	BOOST_ASSERT( !callSizes.empty() && callSizes.front() == size );
	callSizes.pop_front();
#endif // ASSERTS_ENABLED == 1

	uint8 *chunk = topChunk;
	Header *header = getHeader( topChunk );
	// If top is empty, move to the prev
	// We leave 1 empty chunk as padding
	if (header->firstUsedOffset == header->lastUsedOffset) {
		// Empty top chunks should not have space on bottom
		BOOST_ASSERT( header->firstUsedOffset == FIRST_VALID_OFFSET );
		chunk = header->prevChunk;
		header = getHeader( chunk );
	}

	// Add space back
	BOOST_ASSERT( header->firstUsedOffset + size <= header->lastUsedOffset );
	header->lastUsedOffset -= size;
	if (header->firstUsedOffset == header->lastUsedOffset) {
		// Chunk is empty - push both first and last to the bottom
		header->firstUsedOffset = header->lastUsedOffset = FIRST_VALID_OFFSET;

		// If this isn't the top chunk, it means we have two empty chunks, so deallocate the current top
		header->nextChunk = NULL;
		delete[] topChunk;
		topChunk = chunk;
	}
}
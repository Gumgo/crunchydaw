#include "ChangeManager.h"

ChangeManager::ChangeManager()
	: actionStack( 1024 * 1024 ) { // 1MB chunk size
	maxUndos = 0;
	currentPosition = 0;
	savePosition = UNSAVED;

	nestCount = 0;
	performingUndoOrRedo = false;
}

ChangeManager::~ChangeManager() {
	for (size_t i = 0; i < changeStack.size(); ++i) {
		popTopChange();
	}
}

void ChangeManager::setMaxUndos( uint32 maxUndos ) {
	this->maxUndos = maxUndos;
	// If we have too many undos, pop them (unless we hit our current position)
	while (changeStack.size() > maxUndos && currentPosition > 0) {
		popBottomChange();
		--currentPosition;
		// Clear save if it runs off the bottom
		if (savePosition != UNSAVED && savePosition > 0) {
			--savePosition;
		} else {
			savePosition = UNSAVED;
		}
	}

	BOOST_ASSERT( currentPosition == 0 || changeStack.size() == maxUndos );
}

uint32 ChangeManager::getMaxUndos() const {
	return maxUndos;
}

void ChangeManager::beginChange() {
	BOOST_ASSERT( !performingUndoOrRedo );

	// If save position was higher in the stack, clear it
	if (savePosition != UNSAVED && savePosition > currentPosition) {
		savePosition = UNSAVED;
	}

	// Clear the top of the change stack
	while (changeStack.size() > currentPosition) {
		popTopChange();
	}

	BOOST_ASSERT( currentPosition == changeStack.size() );

	if (nestCount == 0) {
		// Create a new change
		Change change;
		change.firstAction = NULL;
		currentTopAction = NULL;
		changeStack.push_back( change );
	}

	++nestCount;
}

void ChangeManager::endChange() {
	BOOST_ASSERT( !performingUndoOrRedo );
	BOOST_ASSERT( nestCount > 0 );

	--nestCount;
	if (nestCount == 0) {
		// We just ended the final nesting of the change, so move up one
		++currentPosition;
	}
}

bool ChangeManager::isPerformingUndoOrRedo() const {
	return performingUndoOrRedo;
}

void ChangeManager::saveAtCurrentPosition() {
	// We're not allowed to save while building a change or performing an undo/redo
	BOOST_ASSERT( nestCount == 0 );
	BOOST_ASSERT( !performingUndoOrRedo );

	savePosition = currentPosition;
}

bool ChangeManager::isSaved() const {
	return savePosition == currentPosition;
}

uint32 ChangeManager::getCurrentChangeStackSize() const {
	return changeStack.size();
}

uint32 ChangeManager::getCurrentPosition() const {
	return currentPosition;
}

uint32 ChangeManager::getSavePosition() const {
	return savePosition;
}

void ChangeManager::clearChangeStack( bool save ) {
	BOOST_ASSERT( nestCount == 0 );
	BOOST_ASSERT( !performingUndoOrRedo );

	while (changeStack.size() > 0) {
		popTopChange();
	}

	currentPosition = 0;
	if (save) {
		savePosition = 0;
	} else {
		savePosition = UNSAVED;
	}
}

bool ChangeManager::canPerformUndo() const {
	return currentPosition > 0;
}

bool ChangeManager::canPerformRedo() const {
	return currentPosition < changeStack.size();
}

bool ChangeManager::undo() {
	if (!canPerformUndo()) {
		return false;
	}

	// Execute all UNDO Actions
	ActionBase *action = changeStack[currentPosition].firstAction;
	// Execute the list in reverse
	if (action) {
		while (action->next) {
			action = action->next;
		}
	}

	while (action) {
		// Call the redo wrapper, which will cast action to the appropriate type and then call redo
		(*action->actionData->callUndo)( action );
		action = action->prev;
	}

	--currentPosition;
	return true;
}

bool ChangeManager::redo() {
	if (!canPerformRedo()) {
		return false;
	}

	// Execute all REDO Actions
	ActionBase *action = changeStack[currentPosition].firstAction;
	while (action) {
		// Call the redo wrapper, which will cast action to the appropriate type and then call redo
		(*action->actionData->callRedo)( action );
		action = action->next;
	}

	++currentPosition;
	return true;
}

void ChangeManager::popBottomChange() {
	BOOST_ASSERT( !changeStack.empty() );

	Change &change = changeStack.front();

	// Deallocate each Action in the list - forward order because we are at the bottom of the stack
	ActionBase *currentAction = change.firstAction;
	while (currentAction) {
		ActionBase *action = currentAction;
		currentAction = action->next;

		size_t alignedSize = align( action->actionData->size, ChunkedStack::ALIGNMENT_BITS );
		// Call the destructor "virtually"
		(*action->actionData->callDerivedDestructor)( action );
		actionStack.popBottom( alignedSize );
	}

	changeStack.pop_front();
}

void ChangeManager::popTopChange() {
	BOOST_ASSERT( !changeStack.empty() );

	Change &change = changeStack.back();

	// Deallocate each Action in the list - reverse order because we are at the top of the stack
	ActionBase *currentAction = change.firstAction;
	// Find the last action in the list
	if (currentAction) {
		while (currentAction->next) {
			currentAction = currentAction->next;
		}
	}

	while (currentAction) {
		ActionBase *action = currentAction;
		currentAction = action->prev;

		size_t alignedSize = align( action->actionData->size, ChunkedStack::ALIGNMENT_BITS );
		// Call the destructor "virtually"
		(*action->actionData->callDerivedDestructor)( action );
		actionStack.popBottom( alignedSize );
	}

	changeStack.pop_back();
}

void ChangeManager::addToActionList( ActionBase *action ) {
	action->prev = currentTopAction;
	if (!currentTopAction) {
		Change &change = changeStack[currentPosition];
		BOOST_ASSERT( !change.firstAction );
		change.firstAction = action;
	} else {
		BOOST_ASSERT( !currentTopAction->next );
		currentTopAction->next = action;
	}
	action->next = NULL;
	currentTopAction = action;
}

ChangeManager::Action<void>::Action(
	Command<void>::Function undoCommand,
	Command<void>::Function redoCommand )
	: undoCommand( undoCommand )
	, redoCommand( redoCommand ) {
}

void ChangeManager::Action<void>::callDestructor( ActionBase *action ) {
	static_cast<Action<void>*>( action )->~Action<void>();
}

void ChangeManager::Action<void>::callUndo( ActionBase *action ) {
	Action<void> *typedAction = static_cast<Action<void>*>( action );
	typedAction->undoCommand( COMMAND_TYPE_UNDO );
}

void ChangeManager::Action<void>::callRedo( ActionBase *action ) {
	Action<void> *typedAction = static_cast<Action<void>*>( action );
	typedAction->redoCommand( COMMAND_TYPE_REDO );
}

void ChangeManager::pushActionAndDo(
	Command<void>::Function doCommand,
	Command<void>::Function undoCommand,
	Command<void>::Function redoCommand ) {
	BOOST_ASSERT( !performingUndoOrRedo );
	BOOST_ASSERT( nestCount > 0 );
	BOOST_ASSERT( !changeStack.empty() );

	// Setup static ActionData - one of these exists per template type
	static const ActionData actionData = {
		sizeof( Action<void> ),			// Size
		&Action<void>::callDestructor,	// Destruction wrapper
		&Action<void>::callUndo,		// Undo wrapper
		&Action<void>::callRedo			// Redo wrapper
	};

	// Allocate space on the actionStack
	size_t alignedSize = align( actionData.size, ChunkedStack::ALIGNMENT_BITS );
	void *actionPtr = actionStack.pushTop( alignedSize );

	// Construct using placement new
	Action<void> *action = new (actionPtr) Action<void>( undoCommand, redoCommand );
	action->actionData = &actionData;

	// Set up action in the list
	addToActionList( action );

	// Last, execute DO
	if (doCommand) {
		doCommand( COMMAND_TYPE_DO );
	}
}

ChangeManager::ChangeScope::ChangeScope( ChangeManager &changeManager )
	: changeManager( changeManager ) {
	changeManager.beginChange();
}

ChangeManager::ChangeScope::~ChangeScope() {
	changeManager.endChange();
}
#include "ApplicationData.h"

ApplicationData::ApplicationData() {
	initialized = false;
}

ApplicationData::~ApplicationData() {
	terminate();
}

bool ApplicationData::initialize() {
	BOOST_ASSERT( !initialized );

	// Read in settings
	settings.load();

	// Apply settings
	changeManager.setMaxUndos( settings.editing.maxUndos );

	initialized = true;
	return true;
}

void ApplicationData::terminate() {
	initialized = false;
}

bool ApplicationData::isInitialized() const {
	return initialized;
}

Settings &ApplicationData::getSettings() {
	return settings;
}

const Settings &ApplicationData::getSettings() const {
	return settings;
}

Project &ApplicationData::getProject() {
	return project;
}

const Project &ApplicationData::getProject() const {
	return project;
}

ChangeManager &ApplicationData::getChangeManager() {
	return changeManager;
}

const ChangeManager &ApplicationData::getChangeManager() const {
	return changeManager;
}
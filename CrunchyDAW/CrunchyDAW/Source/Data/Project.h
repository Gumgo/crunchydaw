/*
 * Project.h
 * ---------
 * Description: Contains all data which is specific to a project.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef PROJECT_H__DEFINED__
#define PROJECT_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/Modules/ModuleData.h"
#include "Source/Common/UuidGenerator.h"

#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

class Project {
public:
	Project();
	~Project();

	bool load( const std::string &fname );
	bool save( const std::string &fname );

	boost::uuids::uuid generateUniqueUuid();

	// Adds a module with the given UUID
	// It is an error to add two modules with the same UUID
	void addModule( ModuleData *module );

	// Removes and returns the module with the given UUID, or NULL if it doesn't exist
	ModuleData *removeModule( const boost::uuids::uuid &uuid );

	// Returns the module with the given UUID, or NULL if it doesn't exist
	ModuleData *getModule( const boost::uuids::uuid &uuid );
	const ModuleData *getModule( const boost::uuids::uuid &uuid ) const;

	// Connects two module slots
	// The following is required (and asserted):
	// - Both modules must exist
	// - Both slot indices must be valid
	// - Both slots must be the same format
	// Returns whether the connection is successfully added to the module graph. Addition of the connection can fail if:
	// - Any of the slots are already in use
	// - Adding the connection would result in a cycle
	bool addModuleSlotConnection( const ModuleDataSlotConnection &connection );

private:
	// Keeps track of UUIDs
	UuidGenerator uuidGenerator;

	// Map of UUID -> Module
	typedef boost::unordered_map<boost::uuids::uuid, ModuleData *> ModuleMap;
	ModuleMap modules;

	// Map of Slot -> Slot for slot connections
	typedef boost::unordered_map<ModuleDataSlotIdentifier, ModuleDataSlotIdentifier> ModuleSlotMap;
	ModuleSlotMap moduleSlotConnections;
};

#endif // PROJECT_H__DEFINED__
/*
 * ChunkedStack.h
 * --------------
 * Description: A stack capable of allocating variable-sized elements.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef CHUNKED_STACK_H__DEFINED__
#define CHUNKED_STACK_H__DEFINED__

#include "Source/Common/Common.h"

#if ASSERTS_ENABLED == 1
#include <deque>
#endif // ASSERTS_ENABLED == 1

// ChunkedStack is a data structure where blocks of bytes can be pushed/popped from either side. ChunkedStack grows and
// shrinks dynamically by allocating large "chunks" at a time, specified at initialization by chunkSize. To avoid
// unnecessary allocations/deallocations, 1 chunk of padding is left when bytes are popped from either end.
class ChunkedStack {
public:
	ChunkedStack( size_t chunkSize );
	~ChunkedStack();

	// All pushes/pops are 16-byte aligned, return pointer to beginning of block
	void *pushBottom( size_t size );
	void popBottom( size_t size );
	void *pushTop( size_t size );
	void popTop( size_t size );

	static const size_t ALIGNMENT_BITS = 4;
	static const size_t ALIGNMENT = 16;

private:
	// Stack is allocated in "chunks" of size at least chunkSize (bigger if a push requires)
	BOOST_STATIC_ASSERT( ALIGNMENT == (1 << ALIGNMENT_BITS) );

	size_t chunkSize;

	// At front of each chunk
	struct Header {
		uint8 *prevChunk;
		uint8 *nextChunk;
		size_t firstUsedOffset;
		size_t lastUsedOffset;
		size_t size;
	};

	// Since the header is at the beginning of each chunk, our first block is offset
	static const size_t FIRST_VALID_OFFSET = (sizeof( Header ) + (ALIGNMENT-1)) & (ALIGNMENT-1);

	uint8 *bottomChunk;
	uint8 *topChunk;

	Header *getHeader( uint8 *chunk );
	// Allocates chunk on top or bottom and links it to existing chunks
	void allocateChunkForSize( size_t size, bool top );

#if ASSERTS_ENABLED == 1
	// Used to make sure matching push/pop calls are made
	std::deque<size_t> callSizes;
#endif // ASSERTS_ENABLED == 1
};

#endif // CHUNKED_STACK_H__DEFINED__
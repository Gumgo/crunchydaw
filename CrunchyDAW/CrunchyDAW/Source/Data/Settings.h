/*
 * Settings.h
 * ----------
 * Description: Contains global program settings and ability to read them from file and write default file.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef SETTINGS_H__DEFINED__
#define SETTINGS_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/AudioEngine/SampleFormat.h"

class Settings {
public:
	// Initializes to defaults
	Settings();

	bool save();
	void load();

	// List settings in sections here:

	struct Audio {
		std::wstring deviceName;

		static const uint32 MIN_LATENCY_MS = 1;
		static const uint32 MAX_LATENCY_MS = 500;
		uint32 latencyMs;

		static const uint32 MIN_UPDATE_INTERVAL_MS = 1;
		static const uint32 MAX_UPDATE_INTERVAL_MS = 250;
		uint32 updateIntervalMs;

		uint32 sampleRate;

		static const uint32 MIN_CHANNELS = 1;
		static const uint32 MAX_CHANNELS = 4;
		uint32 channels;

		SampleFormat sampleFormat;
		bool exclusiveMode;
	} audio;

	struct Editing {
		uint32 maxUndos;
	} editing;
};

#endif // SETTINGS_H__DEFINED__
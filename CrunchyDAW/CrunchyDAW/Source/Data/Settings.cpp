#include "Settings.h"

#include <qsettings.h>

static const char *ORGANIZATION = "Crunch";
static const char *APPLICATION = "CrunchyDAW";

Settings::Settings() {
}

// Functions to read various types with default arguments

int32 readInt32( QSettings &in, const char *keyName, int32 defaultValue,
	int32 minRange = std::numeric_limits<int32>::min(), int32 maxRange = std::numeric_limits<int32>::max() ) {
	QVariant var = in.value( keyName, QVariant( defaultValue ) );
	bool ok;
	int32 result = var.toInt( &ok );
	if (!ok || result < minRange || result > maxRange) {
		result = defaultValue;
	}
	return result;
}

uint32 readUint32( QSettings &in, const char *keyName, uint32 defaultValue,
	uint32 minRange = std::numeric_limits<uint32>::min(), uint32 maxRange = std::numeric_limits<uint32>::max() ) {
	QVariant var = in.value( keyName, QVariant( defaultValue ) );
	bool ok;
	uint32 result = var.toUInt( &ok );
	if (!ok || result < minRange || result > maxRange) {
		result = defaultValue;
	}
	return result;
}

int8 readInt8( QSettings &in, const char *keyName, int8 defaultValue ) {
	return static_cast<int8>( readInt32( in, keyName, defaultValue,
		std::numeric_limits<int8>::min(), std::numeric_limits<int8>::max() ) );
}

uint8 readUint8( QSettings &in, const char *keyName, uint8 defaultValue ) {
	return static_cast<uint8>( readInt32( in, keyName, defaultValue,
		std::numeric_limits<uint8>::min(), std::numeric_limits<uint8>::max() ) );
}

int16 readInt16( QSettings &in, const char *keyName, int16 defaultValue ) {
	return static_cast<int16>( readInt32( in, keyName, defaultValue,
		std::numeric_limits<int16>::min(), std::numeric_limits<int16>::max() ) );
}

uint16 readUint16( QSettings &in, const char *keyName, uint16 defaultValue ) {
	return static_cast<uint16>( readInt32( in, keyName, defaultValue,
		std::numeric_limits<uint16>::min(), std::numeric_limits<uint16>::max() ) );
}

double readDouble( QSettings &in, const char *keyName, double defaultValue,
	double minRange = std::numeric_limits<double>::min(), double maxRange = std::numeric_limits<double>::max() ) {
	QVariant var = in.value( keyName, QVariant( defaultValue ) );
	bool ok;
	double result = var.toDouble( &ok );
	if (!ok || result < minRange || result > maxRange) {
		result = defaultValue;
	}
	return result;
}

std::string readString( QSettings &in, const char *keyName, const std::string &defaultValue ) {
	return in.value( keyName, QVariant( defaultValue.c_str() ) ).toString().toStdString();
}

std::wstring readWString( QSettings &in, const char *keyName, const std::string &defaultValue ) {
	return in.value( keyName, QVariant( defaultValue.c_str() ) ).toString().toStdWString();
}

bool readBool( QSettings &in, const char *keyName, bool defaultValue ) {
	return in.value( keyName, QVariant( defaultValue ) ).toBool();
}

bool Settings::save() {
	QSettings out( QSettings::IniFormat, QSettings::UserScope, ORGANIZATION, APPLICATION );

	{ // Audio
		out.setValue( "audio/deviceName",		QString::fromWCharArray( audio.deviceName.c_str() ) );
		out.setValue( "audio/latencyMs",		audio.latencyMs );
		out.setValue( "audio/updateIntervalMs",	audio.updateIntervalMs );
		out.setValue( "audio/channels",			audio.channels );
		out.setValue( "audio/sampleFormat",		static_cast<uint32>( audio.sampleFormat ) );
		out.setValue( "audio/exclusiveMode",	audio.exclusiveMode );
	}

	{ // Editing
		out.setValue( "editing/maxUndos",		editing.maxUndos );
	}

	out.sync();

	bool result = (out.status() == QSettings::NoError);
	if (!result) {
		appLog().warning( "application/settings" ) << "Failed to save settings";
	}

	return result;
}

void Settings::load() {
	QSettings in( QSettings::IniFormat, QSettings::UserScope, ORGANIZATION, APPLICATION );

	{ // Audio
		audio.deviceName		= readWString( in, "audio/deviceName", "" );
		audio.latencyMs			= readUint32( in, "audio/latencyMs", 100,
			Audio::MIN_LATENCY_MS, Audio::MAX_LATENCY_MS );
		audio.updateIntervalMs	= readUint32( in, "audio/updateIntervalMs", 10,
			Audio::MIN_UPDATE_INTERVAL_MS, Audio::MAX_UPDATE_INTERVAL_MS );

		audio.sampleRate		= readUint32( in, "audio/sampleRate", 0 );
		bool sampleRateFound = false;
		for (size_t i = 0; i < getSampleRateCount(); ++i) {
			if (audio.sampleRate == getSampleRate( i )) {
				sampleRateFound = true;
				break;
			}
		}
		if (!sampleRateFound) {
			audio.sampleRate = 44800;
		}

		audio.channels			= readUint32( in, "audio/channels", 2, Audio::MIN_CHANNELS, Audio::MAX_CHANNELS );
		audio.sampleFormat		= static_cast<SampleFormat>(
			readUint32( in, "audio/sampleFormat", SAMPLE_FORMAT_INT16, 0, SAMPLE_FORMAT_COUNT ) );
		audio.exclusiveMode		= readBool( in, "audio/exclusiveMode", false );
	}

	{ // Editing
		editing.maxUndos		= readUint32( in, "audio/maxUndos", 50 );
	}
}
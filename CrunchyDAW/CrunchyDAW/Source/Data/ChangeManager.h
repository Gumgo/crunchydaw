/*
 * ChangeManager.h
 * ---------------
 * Description: Keeps track of changes made and provides an undo/redo mechanism.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef CHANGE_MANAGER_H__DEFINED__
#define CHANGE_MANAGER_H__DEFINED__

#include "Source/Common/Common.h"
#include "ChunkedStack.h"

#include <deque>

// Terminology:
// Command: a function which should perform some state change on the project. A Command takes a command Type (either DO,
//     UNDO, or REDO) and a context, which can be of any type and holds data specific to the Action the Command is in.
// Action: a group of three Commands, one of each Type (DO, UNDO, and REDO). The Action can contain a context to store
//     which project state should change when executing Commands.
// Change: a sequence of Actions, executed in order, representing an atomic change of project state. To execute a Change
//     of a given Type means to run through the list of Actions in that change and perform each Command. DO and REDO
//     Change execution is performed in regular order and UNDO Change execution is performed in reverse order.

// The ChangeManager should be used to perform project state changes rather than directly altering project state. To use
// the ChangeManager, beginChange() must first be called. Next, one or more Actions are pushed to the Change by calling
// pushAndDo(). pushAndDo() takes a Command of each type and a context if necessary. Each time pushAndDo() is called,
// the provided DO Command is immediately executed. Finally, the Change is completed by calling endChange().

// For convenience, Changes can be nested. For example, it is valid for function A to call beginChange() and push a DO
// Command which itself calls beginChange() and pushes more Actions. In this case, function A will be called before the
// nested Command functions. The important thing is that endChange() is called the same number of times as
// beginChange(). To ensure this matching beginChange()/endChange() calls, the ChangeScope class should be used.

// Changes are stored in a stack, and Commands of type UNDO and REDO are used to move the current position down and up
// in that stack, respectively (newer means more recent changes). In terms of project state, this means reverting and
// reperforming state changes. When calling the initial beginChange() for a Change, all higher Changes in the stack are
// discarded, and when calling its corresponding endChange(), the current position is set to the top of the stack.

// When UNDO and REDO Commands are being performed, it is illegal to call beginChange(), endChange(), or
// pushActionAndDo(). (In contrast, these functions are legal to call during a DO Command, as covered above.) If these
// functions are called at the wrong time, an assertion will be triggered if assertions are enabled. The function
// isPerformingUndoOrRedo() can be queried to avoid this situation.

// The Change stack can be either limited or unlimited, and its size can be changed at any time. When shrinking the
// Change stack, only potential UNDOs will ever be removed. It is also possible to clear the stack.

// In addition to the concept of the current position, there also exists a saved position. The saved position is
// initially NO_INDEX, and can be set to the current position by calling saveAtCurrentPosition(). Logically, this should
// be called whenever the user saves the project. If the saved position is equal to the current position it indicates
// that the project has no unsaved changes. This can be queried by calling isSaved().

class ChangeManager {
public:
	enum CommandType {
		COMMAND_TYPE_DO,
		COMMAND_TYPE_UNDO,
		COMMAND_TYPE_REDO
	};

	// A command typed for a context
	template<typename C>
	struct Command {
		typedef fastdelegate::FastDelegate2<CommandType, const C &> Function;
	};

	// A Command typed for no context
	template<>
	struct Command<void> {
		typedef fastdelegate::FastDelegate1<CommandType> Function;
	};


	ChangeManager();
	~ChangeManager();

	void setMaxUndos( uint32 maxUndos );
	uint32 getMaxUndos() const;
	static const uint32 UNLIMITED = boost::integer_traits<uint32>::const_max;

	// Begins a (possibly nested) change
	void beginChange();
	// Ends a (possibly nested) change
	void endChange();

	template<typename C>
	void pushActionAndDo(
		typename Command<C>::Function doCommand,
		typename Command<C>::Function undoCommand,
		typename Command<C>::Function redoCommand,
		const C &context );

	void pushActionAndDo(
		Command<void>::Function doCommand,
		Command<void>::Function undoCommand,
		Command<void>::Function redoCommand );

	// Returns whether an UNDO or REDO is currently being performed
	// Changes should not be built while this is true
	bool isPerformingUndoOrRedo() const;

	void saveAtCurrentPosition();
	bool isSaved() const;

	uint32 getCurrentChangeStackSize() const;
	uint32 getCurrentPosition() const;
	uint32 getSavePosition() const;
	static const uint32 UNSAVED = boost::integer_traits<uint32>::const_max;

	// Clears the change stack, marking the new current position as saved if desired
	void clearChangeStack( bool save );

	// Returns whether there are more undos/redos to perform
	bool canPerformUndo() const;
	bool canPerformRedo() const;

	// Performs undos/redos if there are more to perform
	bool undo();
	bool redo();

	// Enforces matching beginChange()/endChange() calls within a scope
	class ChangeScope : private boost::noncopyable {
	public:
		ChangeScope( ChangeManager &changeManager );
		~ChangeScope();

	private:
		ChangeManager &changeManager;
	};

private:
	class ActionBase;

	// Instantiated statically; holds data about a typed Action
	// Used as a very limited "vtable"
	struct ActionData {
		size_t size;
		// Function to call derived destructor on base pointer
		void (*callDerivedDestructor)( ActionBase * );
		void (*callUndo)( ActionBase * );
		void (*callRedo)( ActionBase * );
	};

	// Base class for Actions
	struct ActionBase {
		const ActionData *actionData;	// Pointer to data about action class
		ActionBase *prev;				// Pointer to previous Action in actionStack, or NULL
		ActionBase *next;				// Pointer to next Action in actionStack, or NULL
	};

	// Holds typed Action data
	template<typename C>
	struct Action : public ActionBase {
		typename Command<C>::Function undoCommand;
		typename Command<C>::Function redoCommand;
		C context;

		Action(
			typename Command<C>::Function undoCommand,
			typename Command<C>::Function redoCommand,
			const C &context );

		static void callDestructor( ActionBase *action );
		static void callUndo( ActionBase *action );
		static void callRedo( ActionBase *action );
	};

	template<>
	struct Action<void> : public ActionBase {
		Command<void>::Function undoCommand;
		Command<void>::Function redoCommand;

		Action(
			Command<void>::Function undoCommand,
			Command<void>::Function redoCommand );

		static void callDestructor( ActionBase *action );
		static void callUndo( ActionBase *action );
		static void callRedo( ActionBase *action );
	};

	// Holds Action data
	ChunkedStack actionStack;

	struct Change {
		// Pointer to the first Action in the Action list, allocated from the actionStack
		ActionBase *firstAction;

		// Actions in the ActionList are stored consecutively in the actionStack. Each Action is allocated with 16-byte
		// alignment. All Actions are derived from the ActionBase class, so to determine the size of an Action, the void
		// pointer is cast an ActionBase pointer and the ActionData is then looked up. ActionBase also contains pointers
		// to the previous and next Actions in the list, or NULL at the list boundaries.

		// Note: in STL terminology, the "bottom" of the stack is the front, and the "top" is the back
	};

	// Stack of changes
	std::deque <Change> changeStack;
	uint32 maxUndos;

	uint32 currentPosition;
	uint32 savePosition;

	size_t nestCount;				// Number of changes we have currently nested
	bool performingUndoOrRedo;		// Whether we are performing an undo or redo
	ActionBase *currentTopAction;	// Points to the current top Action of the Change we're building

	// Functions to pop change stack; takes care of Action list deallocation
	void popBottomChange();
	void popTopChange();

	// Adds an Action to the current Change's Action list
	void addToActionList( ActionBase *action );
};

#include "ChangeManager.inl"

#endif // CHANGE_MANAGER_H__DEFINED__
#include "Project.h"

Project::Project() {
}

Project::~Project() {
}

bool Project::load( const std::string &fname ) {
	// $TODO
	return true;
}

bool Project::save( const std::string &fname ) {
	// $TODO
	return true;
}

boost::uuids::uuid Project::generateUniqueUuid() {
	return uuidGenerator.generateUuid();
}

void Project::addModule( ModuleData *module ) {
	BOOST_ASSERT( module );
	BOOST_ASSERT( modules.find( module->getUuid() ) == modules.end() );

	modules.insert( std::make_pair( module->getUuid(), module ) );
}

ModuleData *Project::removeModule( const boost::uuids::uuid &uuid ) {
	ModuleData *module = NULL;
	ModuleMap::iterator it = modules.find( uuid );
	if (it != modules.end()) {
		module = it->second;
		modules.erase( it );
	}

	return module;
}

ModuleData *Project::getModule( const boost::uuids::uuid &uuid ) {
	ModuleData *module = NULL;
	ModuleMap::iterator it = modules.find( uuid );
	if (it != modules.end()) {
		module = it->second;
	}

	return module;
}

const ModuleData *Project::getModule( const boost::uuids::uuid &uuid ) const {
	const ModuleData *module = NULL;
	ModuleMap::const_iterator it = modules.find( uuid );
	if (it != modules.end()) {
		module = it->second;
	}

	return module;
}

bool Project::addModuleSlotConnection( const ModuleDataSlotConnection &connection ) {
	const ModuleData *outputModule = getModule( connection.outputModuleUuid );
	const ModuleData *inputModule = getModule( connection.inputModuleUuid );

	// Modules must exist
	BOOST_ASSERT( outputModule );
	BOOST_ASSERT( inputModule );

	// Slots must exist
	BOOST_ASSERT( connection.outputSlotIndex < outputModule->getOutputSlotCount() );
	BOOST_ASSERT( connection.inputSlotIndex < inputModule->getInputSlotCount() );

	// Slots must be the same format
	BOOST_ASSERT( outputModule->getOutputSlot( connection.outputSlotIndex ).getFormat() ==
		inputModule->getInputSlot( connection.inputSlotIndex ).getFormat() );


	ModuleDataSlotIdentifier outputSlot;
	outputSlot.moduleUuid = connection.outputModuleUuid;
	outputSlot.slotIndex = connection.outputSlotIndex;
	outputSlot.type = ModuleDataSlotIdentifier::TYPE_OUTPUT;

	ModuleDataSlotIdentifier inputSlot;
	inputSlot.moduleUuid = connection.inputModuleUuid;
	inputSlot.slotIndex = connection.inputSlotIndex;
	inputSlot.type = ModuleDataSlotIdentifier::TYPE_INPUT;

	// Check if either slot is in use
	if (moduleSlotConnections.find( outputSlot ) == moduleSlotConnections.end() ||
		moduleSlotConnections.find( inputSlot ) == moduleSlotConnections.end()) {
		return false;
	}

	// Determine whether adding the connection would create cycles
	bool createsCycle = false; // $TODO
	if (!createsCycle) {
		return false;
	}

	// Add the connection - map out to in and in to out
	moduleSlotConnections.insert( std::make_pair( outputSlot, inputSlot ) );
	moduleSlotConnections.insert( std::make_pair( inputSlot, outputSlot ) );
	return true;
}

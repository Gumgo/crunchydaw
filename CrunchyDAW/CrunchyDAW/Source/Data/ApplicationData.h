/*
 * ApplicationData.h
 * -----------------
 * Description: Keeps track of the underlying state of the application/project, having no knowledge of GUI.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef APPLICATION_DATA_H__DEFINED__
#define APPLICATION_DATA_H__DEFINED__

#include "Source/Common/Common.h"

#include "Settings.h"
#include "Project.h"
#include "ChangeManager.h"

class ApplicationData {
public:
	ApplicationData();
	~ApplicationData();

	bool initialize();
	void terminate();
	bool isInitialized() const;

	Settings &getSettings();
	const Settings &getSettings() const;

	Project &getProject();
	const Project &getProject() const;

	ChangeManager &getChangeManager();
	const ChangeManager &getChangeManager() const;

private:
	bool initialized;				// Whether application data is initialized

	Settings settings;				// The application settings, not specific to a particular project
	Project project;				// State of the current project
	ChangeManager changeManager;	// Keeps track of project history state
};

#endif // APPLICATION_DATA_H__DEFINED__
/*
 * ApplicationBehavior.h
 * ---------------------
 * Description: Defines the behavior of the application by linking together the application data and GUI.
 * REPLACE_WITH_LICENSE_INFO
 */

#ifndef APPLICATION_BEHAVIOR_H__DEFINED__
#define APPLICATION_BEHAVIOR_H__DEFINED__

#include "Source/Common/Common.h"
#include "Source/Data/ChangeManager.h"

#include <boost/uuid/uuid.hpp>

class Application;

// ApplicationBehavior provides the hook mechanism to allow the GUI to drive the underlying application state, and for
// the GUI to be updated when that state changes.
class ApplicationBehavior {
public:
	ApplicationBehavior( Application &app );

	// GLUE FUNCTIONS/SUB-INTERFACES FOLLOW

	bool initialize();
	void terminate();

	bool openAudioDevice( size_t index );
	bool closeAudioDevice();

	// Library
	struct AddModuleToProjectContext {
		boost::uuids::uuid moduleClassUuid;		// UUID of module class to add
		boost::uuids::uuid moduleInstanceUuid;	// UUID of new module instance
		int32 canvasX, canvasY;					// Position of new module
		int32 canvasW, canvasH;					// Size of new module
	};
	void addModuleToProject( const boost::uuids::uuid &moduleUuid );
	void addModuleToProjectCommand( ChangeManager::CommandType commandType, const AddModuleToProjectContext &context );

private:
	Application &app;
};

#endif // APPLICATION_BEHAVIOR_H__DEFINED__
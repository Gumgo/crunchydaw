#include "ApplicationBehavior.h"

#include "Source/Application.h"

#include <boost/uuid/uuid_io.hpp>

// $TEMP
#include <boost/math/constants/constants.hpp>
void provideAudio( const AudioDeviceSettings &settings, uint32 frameCount, uint8 *buffer );

ApplicationBehavior::ApplicationBehavior( Application &app )
	: app( app ) {
	// Set function pointers here
	app.mainWindow.library->addModuleToProjectBehavior.bind( this, &ApplicationBehavior::addModuleToProject );
}

bool ApplicationBehavior::initialize() {
	bool result;

	// Initialize each component of the application in proper order

	app.moduleRegistry.registerModules();

	result = app.applicationData.initialize();

	if (result) {
		result = app.audioEngine.initialize();
	}

	if (result) {
		result = app.mainWindow.initialize();
	}

	if (!result) {
		terminate();
	}

	// Start the audio engine using the default device, if possible
	if (app.audioEngine.getAvailableDevices().empty()) {
		appLog().warning( "audio/device" ) << "No available audio devices detected";
	} else {
		appLog().message( "audio/device" ) <<
			app.audioEngine.getAvailableDevices().size() << " audio devices detected";

		const Settings &settings = app.applicationData.getSettings();
		// Try to find device in the list and open it
		if (!settings.audio.deviceName.empty()) {
			// Find device with the listed name
			for (size_t i = 0; i < app.audioEngine.getAvailableDevices().size(); ++i) {
				if (app.audioEngine.getAvailableDevices()[i].name == settings.audio.deviceName) {
					openAudioDevice( i );
					break;
				}
			}
		}
	}

	return result;
}

void ApplicationBehavior::terminate() {
	// Store settings before terminating so we store the state of the application before shutting down
	if (app.applicationData.isInitialized()) {
		app.applicationData.getSettings().save();
	}

	// Stop the audio thread
	closeAudioDevice();

	// Terminate systems in reverse order as initialization
	app.mainWindow.terminate();
	app.audioEngine.terminate();
	app.applicationData.terminate();
}

bool ApplicationBehavior::openAudioDevice( size_t index ) {
	BOOST_ASSERT( index < app.audioEngine.getAvailableDevices().size() );
	bool result = true;

	// Close current device
	closeAudioDevice();

	// Setup audio device settings
	const Settings &settings = app.applicationData.getSettings();
	AudioDeviceSettings deviceSettings;
	deviceSettings.latencyMs		= settings.audio.latencyMs;
	deviceSettings.updateIntervalMs	= settings.audio.updateIntervalMs;
	deviceSettings.sampleRate		= settings.audio.sampleRate;
	deviceSettings.channels			= settings.audio.channels;
	deviceSettings.sampleFormat		= settings.audio.sampleFormat;
	deviceSettings.exclusiveMode	= settings.audio.exclusiveMode;
	deviceSettings.mainWindowId		= app.mainWindow.winId();

	result = app.audioEngine.openAudioDevice( index, deviceSettings,
		AudioDevice::AudioEventHandler(), // $TODO
		&provideAudio ); // $TODO

	if (result) {
		const AudioDevice *device = app.audioEngine.getCurrentAudioDevice();

		// Store the name in settings so we default to that device
		app.applicationData.getSettings().audio.deviceName = device->getDeviceInfo().name;
	}

	return true;
}

bool ApplicationBehavior::closeAudioDevice() {
	bool result = app.audioEngine.closeCurrentAudioDevice();
	if (result) {
		app.applicationData.getSettings().audio.deviceName.clear();
	}

	return result;
}

void ApplicationBehavior::addModuleToProject( const boost::uuids::uuid &moduleUuid ) {
	appLog().message( "canvas" ) << "Adding module of class " << moduleUuid;

	{
		ChangeManager::ChangeScope( app.applicationData.getChangeManager() );
		// $TODO this interface is really verbose and doesn't actually work - fix it!
		//app.applicationData.getChangeManager().pushActionAndDo(
		//	ChangeManager::Command<AddModuleToProjectContext>::Function( this, &ApplicationBehavior::addModuleToProjectCommand ),
		//	ChangeManager::Command<AddModuleToProjectContext>::Function( this, &ApplicationBehavior::addModuleToProjectCommand ),
		//	ChangeManager::Command<AddModuleToProjectContext>::Function( this, &ApplicationBehavior::addModuleToProjectCommand ),
		//	context );
	}
}

void ApplicationBehavior::addModuleToProjectCommand(
	ChangeManager::CommandType commandType, const AddModuleToProjectContext &context ) {
	if (commandType == ChangeManager::COMMAND_TYPE_DO ||
		commandType == ChangeManager::COMMAND_TYPE_REDO) {
		// .. add the module
	} else {
		BOOST_ASSERT( commandType == ChangeManager::COMMAND_TYPE_UNDO );
		// .. remove the module
	}
}


// $TEMP
void provideAudio( const AudioDeviceSettings &settings, uint32 frameCount, uint8 *buffer ) {
	// $TEMP
	static uint32 count = 0;

	double hz = 440.0f;
	double freq = 1.0 / hz;
	for (uint32 i = 0; i < frameCount; ++i) {
		uint32 s = count;

		double time = (double)s / (double)settings.sampleRate;
		double ratio = fmod( time, freq ) * hz; // 0 to 1
		double rads = ratio * 2.0f * boost::math::constants::pi<double>();
		double smp = 0.0f; //sin( rads ); // uncomment for sine wave

		for (uint32 c = 0; c < settings.channels; ++c) {
			uint32 index = i * settings.channels + c;
			switch (settings.sampleFormat) {
			case SAMPLE_FORMAT_UINT8:
				buffer[index] = (uint8)((smp + 1.0f) * 127.5);
				break;
			case SAMPLE_FORMAT_INT16:
				((int16*)buffer)[index] = (int16)(smp * 32767.5 - 0.5);
				break;
			case SAMPLE_FORMAT_INT24:
				BOOST_ASSERT_MSG( false, "No 24 bit int type, blah" );
				break;
			case SAMPLE_FORMAT_INT32:
				((int32*)buffer)[index] = (int32)(smp * 2147483647.5 - 0.5);
				break;
			case SAMPLE_FORMAT_FLOAT32:
				((float*)buffer)[index] = (float)smp;
				break;
			}
		}

		++count;
	}
}